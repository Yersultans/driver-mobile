import * as React from 'react'
import { Dimensions } from 'react-native'
import Svg, { Path } from 'react-native-svg'

const { width } = Dimensions.get('window')

function ArrowLeft(props) {
  return (
    <Svg
      width={width > 400 ? 24 : 20}
      height={width > 400 ? 24 : 20}
      viewBox="0 0 24 24"
      fill="none"
      {...props}
    >
      <Path
        d="M5.744 12.857l10.792 10.789c.474.472 1.241.472 1.716 0a1.209 1.209 0 000-1.712L8.315 12l9.936-9.933a1.21 1.21 0 000-1.714 1.216 1.216 0 00-1.716 0L5.742 11.144a1.223 1.223 0 00.002 1.713z"
        fill={props.color || '#000'}
      />
    </Svg>
  )
}

export default ArrowLeft

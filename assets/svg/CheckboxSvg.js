import * as React from 'react'
import Svg, { Rect, Circle, Path } from 'react-native-svg'
import PropTypes from 'prop-types'

function CheckboxSvg(props) {
  return (
    <Svg
      width={props.size}
      height={props.size}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Rect
        width={props.size}
        height={props.size}
        rx={props.radius}
        fill={props.color}
      />
      <Circle cx={16} cy={16} r={16} fill="#fff" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M23.273 12.94l-8 8c-.26.26-.6.393-.94.393-.34 0-.68-.133-.94-.393l-4.666-4.667a1.316 1.316 0 010-1.88 1.316 1.316 0 011.88 0l3.726 3.72 7.06-7.053a1.316 1.316 0 011.88 0c.527.52.527 1.36 0 1.88zM16 0C7.167 0 0 7.167 0 16s7.167 16 16 16 16-7.167 16-16S24.833 0 16 0z"
        fill={props.color}
      />
    </Svg>
  )
}
CheckboxSvg.propTypes = {
  size: PropTypes.number,
  radius: PropTypes.number,
  color: PropTypes.string
}
CheckboxSvg.defaultProps = {
  size: 32,
  radius: 8,
  color: '#6F2CFF'
}

export default CheckboxSvg

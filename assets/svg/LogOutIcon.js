import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

function HidePasswordIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="none"
      viewBox="0 0 32 32"
    >
      <Path
        fill="#000"
        d="M16 4c-4.05 0-7.64 2.012-9.813 5.094l1.625 1.156A9.985 9.985 0 0116 6c5.535 0 10 4.465 10 10s-4.465 10-10 10a9.99 9.99 0 01-8.188-4.25l-1.625 1.156A11.987 11.987 0 0016 28c6.617 0 12-5.383 12-12S22.617 4 16 4zm-7.344 7.281l-4 4L3.97 16l.687.719 4 4 1.438-1.438L7.813 17H20v-2H7.812l2.282-2.281-1.438-1.438z"
      />
    </Svg>
  )
}

export default HidePasswordIcon

import React from 'react'
import { View, Text, StyleSheet, Alert, TouchableOpacity } from 'react-native'
import { AntDesign, FontAwesome5 } from '@expo/vector-icons'
import PropTypes from 'prop-types'
import moment from 'moment'
import localization from 'moment/locale/ru'

import THEME from '../shared/theme'

const CardItem = ({ item, onDelete }) => {
  moment.updateLocale('ru', localization)
  const handleDelete = () => {
    Alert.alert('Warning', 'You want to delete this address?', [
      {
        text: 'No',
        style: 'cancel'
      },
      { text: 'Yes', onPress: () => onDelete(item.id) }
    ])
  }
  return (
    <View style={styles.container} key={item?.id}>
      <View style={styles.info}>
        {item?.type === 'visa' ? (
          <FontAwesome5 name="cc-visa" size={24} color="black" />
        ) : (
          <FontAwesome5 name="cc-mastercard" size={24} color="black" />
        )}
        <View
          style={{
            width: 230,
            marginLeft: 16
          }}
        >
          <Text style={styles.street}>{item.number}</Text>
          <Text style={styles.otherInfo}>{item.name}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={handleDelete} style={styles.info}>
        <AntDesign name="delete" size={24} color={THEME.COLOR_MAIN} />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME.COLOR_WHITE,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 20,
    marginBottom: 20
  },
  info: {
    padding: 16,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  street: {
    lineHeight: 18,
    fontSize: 20,
    fontFamily: THEME.MEDIUM,
    marginBottom: 8
  },
  otherInfo: {
    lineHeight: 16,
    fontSize: 16,
    fontFamily: THEME.REGULAR
  }
})

CardItem.propTypes = {
  item: PropTypes.objectOf(PropTypes.any).isRequired,
  onDelete: PropTypes.func.isRequired
}

export default CardItem

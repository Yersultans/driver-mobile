import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { gql, useQuery, useMutation } from '@apollo/client'
import moment from 'moment'
import PropTypes from 'prop-types'
import { AntDesign } from '@expo/vector-icons'
import localization from 'moment/locale/ru'
import HeaderNav from '../shared/HeaderNav'
import Cards from './Cards.desing'
import RegistrationCardModal from './RegistrationCardModal'

import StatusBarCustom from '../shared/StatusBarCustom'
import Loading from '../shared/Loading'
import THEME from '../shared/theme'

const GET_CARDS = gql`
  query userCards($userId: ID) {
    userCards(userId: $userId) {
      id
      name
      expDate
      cvv
      type
      number
      amount
    }
  }
`

const DELETE_CARD = gql`
  mutation deleteCard($id: ID!) {
    deleteCard(id: $id)
  }
`

const ADD_CARD = gql`
  mutation addCard($input: CardInput) {
    addCard(input: $input) {
      id
      name
      expDate
      cvv
      type
      number
      amount
    }
  }
`
const CardsContainer = ({ route }) => {
  const { userId } = route.params
  moment.updateLocale('ru', localization)
  const navigation = useNavigation()
  const [userCards, setUserCards] = React.useState(null)
  const [registrationModal, setRegistrationModal] = React.useState(false)

  const { data, loading, error, refetch } = useQuery(GET_CARDS, {
    fetchPolicy: 'no-cache',
    variables: {
      userId
    }
  })
  const [deleteCard] = useMutation(DELETE_CARD)
  const [addCard] = useMutation(ADD_CARD, {
    onCompleted() {
      refetch()
    }
  })

  React.useEffect(() => {
    if (data) {
      setUserCards(data.userCards)
    }
  }, [data, loading, error])

  const handleDelete = id => {
    deleteCard({
      variables: {
        id
      }
    })
    setUserCards(userCards.filter(card => card.id !== id))
    refetch()
  }

  const handleAdd = values => {
    addCard({
      variables: {
        input: { ...values, user: userId }
      },
      errorPolicy: 'all'
    })
    refetch()
  }

  return (
    <>
      <StatusBarCustom />
      <HeaderNav
        title="My Cards"
        backgroundColor={THEME.COLOR_WHITE}
        onPress={() => navigation.goBack()}
        withNextBtn
        onPressNext={() => {
          setRegistrationModal(true)
        }}
        nextBtnTitle={<AntDesign name="plus" size={24} color="black" />}
      />
      {loading ? (
        <Loading />
      ) : (
        <Cards userCards={userCards} onDelete={handleDelete} />
      )}
      <RegistrationCardModal
        onAdd={handleAdd}
        open={registrationModal}
        setOpen={setRegistrationModal}
      />
    </>
  )
}

CardsContainer.propTypes = {
  route: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)).isRequired
}

export default CardsContainer

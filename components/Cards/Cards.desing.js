import React from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import PropTypes from 'prop-types'
import moment from 'moment'
import localization from 'moment/locale/ru'
import CardItem from './CardItem'

import THEME from '../shared/theme'

const Cards = ({ userCards, onDelete }) => {
  moment.updateLocale('ru', localization)
  return (
    <View style={styles.container}>
      <FlatList
        data={userCards}
        style={styles.sectionList}
        contentContainerStyle={styles.sectionContainer}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id}
        renderItem={({ item }) => <CardItem item={item} onDelete={onDelete} />}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME.COLOR_WHITE,
    justifyContent: 'space-between'
  },
  sectionList: {
    paddingHorizontal: 24
  },
  restaurantContainer: {
    flex: 1,
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderColor: THEME.COLOR_WHITE
  },
  image: {
    height: 72,
    width: 104,
    borderRadius: 8
  },
  inCont: {
    flexDirection: 'row',
    marginTop: 24,
    marginBottom: 28
  },
  restaurantInfo: {
    paddingHorizontal: 16,
    alignSelf: 'center'
  },
  restaurnatHeader: {
    paddingBottom: 10,
    lineHeight: 16,
    fontSize: 16,
    fontFamily: THEME.MEDIUM
  }
})

Cards.propTypes = {
  userCards: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)).isRequired,
  onDelete: PropTypes.func.isRequired
}

export default Cards

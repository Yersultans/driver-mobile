import React from 'react'
import { Modal, Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import {
  CreditCardInput,
  LiteCreditCardInput
} from 'react-native-credit-card-input'
import PropTypes from 'prop-types'
import { Entypo } from '@expo/vector-icons'
import THEME from '../shared/theme'
import Button from '../shared/Button'

const RegistrationCardModal = ({ onAdd, open, setOpen }) => {
  const [values, setValues] = React.useState(null)
  const [isValid, setIsValid] = React.useState(false)
  return (
    <Modal
      visible={open}
      transparent
      animationType="fade"
      onRequestClose={() => {
        setOpen(false)
        setValues(null)
      }}
    >
      <TouchableOpacity style={styles.container} onPress={() => setOpen(false)}>
        <View style={styles.modal}>
          <CreditCardInput
            requiresName
            onChange={form => {
              if (
                form.values.name &&
                form.values.number &&
                form.values.type &&
                form.values.cvc &&
                form.values.expiry
              ) {
                setIsValid(true)
                setValues({
                  name: form.values.name.toUpperCase(),
                  number: form.values.number,
                  type: form.values.type,
                  cvv: form.values.cvc,
                  expDate: form.values.expiry
                })
              } else {
                setIsValid(false)
                setValues(null)
              }
            }}
          />
          <Button
            onPress={() => {
              if (values) {
                onAdd(values)
                setOpen(false)
                setValues(null)
              }
            }}
            propStyles={{
              marginTop: 24,
              marginHorizontal: 24
            }}
            disabled={!isValid}
            paddingVertical={24}
            color={isValid ? THEME.COLOR_WHITE : THEME.COLOR_BLACK_OPACITY_5}
            backgroundColor={
              isValid ? THEME.COLOR_MAIN : THEME.COLOR_GRAY_OPACITY_05
            }
          >
            Registration
          </Button>
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    backgroundColor: THEME.COLOR_BLACK_OPACITY_5
  },
  modal: {
    backgroundColor: THEME.COLOR_WHITE,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    paddingVertical: 24
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  }
})

RegistrationCardModal.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired
}

export default RegistrationCardModal

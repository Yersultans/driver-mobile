import React from 'react'
import { StyleSheet, Dimensions, View, Text } from 'react-native'
import MapView from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import { FontAwesome } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import StatusBarCustom from '../shared/StatusBarCustom'
import THEME from '../shared/theme'
import HeaderNav from '../shared/HeaderNav'

const { Marker } = MapView
const GOOGLE_MAPS_APIKEY = 'AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4'
const mapRegion = {
  latitude: 43.238949,
  longitude: 76.889709,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
}

const DetailsOrderContainer = ({ route }) => {
  const navigation = useNavigation()
  const { order } = route.params

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <HeaderNav
        title="Order Details"
        backgroundColor={THEME.COLOR_WHITE}
        onPress={() => navigation.goBack()}
      />
      <View
        style={{
          marginHorizontal: 8,
          marginVertical: 8,
          borderRadius: 8,
          paddingHorizontal: 16,
          paddingVertical: 16,
          backgroundColor: '#fff'
        }}
      >
        <MapView
          style={styles.map}
          initialRegion={mapRegion}
          showsUserLocation
          showsMyLocationButton
        >
          {order?.formAddress && order?.toAddress && (
            <MapViewDirections
              origin={{
                latitude: order?.formAddress?.latitude,
                longitude: order?.formAddress?.longitude
              }}
              destination={{
                latitude: order?.toAddress?.latitude,
                longitude: order?.toAddress?.longitude
              }}
              lineDashPattern={[1]}
              apikey={GOOGLE_MAPS_APIKEY} // insert your API Key here
              strokeWidth={4}
              strokeColor="green"
            />
          )}
          {order?.formAddress && (
            <Marker
              key={order?.formAddress?.street}
              coordinate={{
                latitude: order?.formAddress?.latitude,
                longitude: order?.formAddress?.longitude
              }}
              pinColor={THEME.COLOR_GOLD}
            />
          )}
          {order?.toAddress && (
            <Marker
              key={order?.toAddress?.street}
              coordinate={{
                latitude: order?.toAddress?.latitude,
                longitude: order?.toAddress?.longitude
              }}
            />
          )}
        </MapView>
      </View>
      <View
        style={{
          marginHorizontal: 8,
          marginVertical: 8,
          borderRadius: 8,
          paddingHorizontal: 16,
          paddingVertical: 16,
          backgroundColor: '#fff'
        }}
      >
        <View
          style={{
            paddingVertical: 16,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: THEME.COLOR_BLACK_OPACITY_05
          }}
        >
          <FontAwesome
            name="dot-circle-o"
            size={16}
            color={THEME.COLOR_GOLD}
            style={{
              marginRight: 8
            }}
          />
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            {order?.formAddress?.street}
          </Text>
        </View>
        <View
          style={{
            paddingVertical: 16,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: THEME.COLOR_BLACK_OPACITY_05
          }}
        >
          <FontAwesome
            name="dot-circle-o"
            size={16}
            color="#FF5C5C"
            style={{
              marginRight: 8
            }}
          />
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            {order?.toAddress?.street}
          </Text>
        </View>
        <View
          style={{
            paddingVertical: 16,
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}
        >
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            Price
          </Text>
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            {order?.price}₸
          </Text>
        </View>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width - 48,
    height: 240
  }
})
export default DetailsOrderContainer

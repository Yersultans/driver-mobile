import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  FlatList
} from 'react-native'
import { FontAwesome5, SimpleLineIcons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'
import moment from 'moment'
import localization from 'moment/locale/ru'
import OrderItem from './OrderItem'

import THEME from '../shared/theme'

const Orders = ({ orders }) => {
  moment.updateLocale('ru', localization)

  return (
    <>
      <View style={styles.container}>
        <FlatList
          data={orders}
          style={styles.sectionList}
          contentContainerStyle={styles.sectionContainer}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id}
          renderItem={({ item, index }) => (
            <OrderItem item={item} isLast={index + 1 === orders.length} />
          )}
        />
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  sectionList: {
    paddingHorizontal: 24,
    paddingBottom: 64
  },
  searchContainer: {},
  orderContainer: {
    flex: 1,
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.05)'
  },
  image: {
    height: 72,
    width: 104,
    borderRadius: 8
  },
  inCont: {
    flexDirection: 'row',
    marginTop: 24,
    marginBottom: 28
  },
  orderInfo: {
    paddingHorizontal: 16,
    alignSelf: 'center'
  },
  restaurnatHeader: {
    paddingBottom: 10,
    lineHeight: 16,
    fontSize: 16,
    fontFamily: THEME.MEDIUM
  },
  buttonConatiner: {
    marginHorizontal: 16,
    marginBottom: 8,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 18,
    lineHeight: 20
  },
  otherText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 18,
    lineHeight: 20
  }
})

Orders.propTypes = {
  orders: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)).isRequired,
  items: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)).isRequired,
  sum: PropTypes.number.isRequired
}

export default Orders

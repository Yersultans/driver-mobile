import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { gql, useQuery } from '@apollo/client'
import moment from 'moment'
import localization from 'moment/locale/ru'
import HeaderNav from '../shared/HeaderNav'
import Orders from './HistoryOrder.design'

import StatusBarCustom from '../shared/StatusBarCustom'
import Loading from '../shared/Loading'

const GET_ORDERS = gql`
  query userOrders($userId: ID) {
    userOrders(userId: $userId) {
      id
      formAddress {
        street
        latitude
        longitude
      }
      toAddress {
        street
        latitude
        longitude
      }
      driver {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      client {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      status
      price
      paymentMethod
    }
  }
`

const OrdersContainer = ({ route }) => {
  const { userId } = route.params
  moment.updateLocale('ru', localization)
  const navigation = useNavigation()
  const [orders, setOrders] = React.useState(null)

  const { data, loading, error, networkStatus } = useQuery(GET_ORDERS, {
    fetchPolicy: 'no-cache',
    variables: {
      userId
    }
  })

  React.useEffect(() => {
    if (data && !loading) {
      setOrders(data.userOrders)
    }
  }, [data, loading, error, networkStatus])

  return (
    <>
      <StatusBarCustom />
      <HeaderNav
        title="Orders"
        backgroundColor="#F5F5F8"
        withBackButton
        onPress={() => navigation.goBack()}
      />
      {loading && orders ? <Loading /> : <Orders orders={orders} />}
    </>
  )
}

export default OrdersContainer

import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { AntDesign, FontAwesome } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import * as Progress from 'react-native-progress'
import PropTypes from 'prop-types'
import moment from 'moment'
import localization from 'moment/locale/en-gb'

import THEME from '../shared/theme'

const OrderItem = ({ item, isLast }) => {
  const navigation = useNavigation()
  moment.updateLocale('en-gb', localization)

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('DetailsOrder', { order: item })
      }}
      style={{ ...styles.restaurantContainer, marginBottom: isLast ? 64 : 20 }}
      key={item?.id}
    >
      <View
        style={{
          marginHorizontal: 8,
          marginVertical: 8,
          borderRadius: 8,
          backgroundColor: '#fff'
        }}
      >
        <View
          style={{
            paddingVertical: 16,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: THEME.COLOR_BLACK_OPACITY_05
          }}
        >
          <FontAwesome
            name="dot-circle-o"
            size={16}
            color={THEME.COLOR_GOLD}
            style={{
              marginRight: 8
            }}
          />
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            {item?.formAddress?.street}
          </Text>
        </View>
        <View
          style={{
            paddingVertical: 16,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: THEME.COLOR_BLACK_OPACITY_05
          }}
        >
          <FontAwesome
            name="dot-circle-o"
            size={16}
            color="#FF5C5C"
            style={{
              marginRight: 8
            }}
          />
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            {item?.toAddress?.street}
          </Text>
        </View>
        <View
          style={{
            paddingVertical: 16,
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}
        >
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            Price
          </Text>
          <Text
            style={{
              fontFamily: THEME.MEDIUM,
              color: THEME.COLOR_BLACK,
              fontSize: 16,
              lineHeight: 16
            }}
          >
            {item?.price}₸
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  restaurantContainer: {
    flex: 1,
    backgroundColor: THEME.COLOR_WHITE,
    borderRadius: 20,
    marginBottom: 20,
    padding: 16
  },
  image: {
    height: 72,
    width: 104,
    borderRadius: 8
  },
  inCont: {
    flexDirection: 'row',
    marginTop: 24,
    marginBottom: 28
  },
  restaurantInfo: {
    paddingHorizontal: 16,
    justifyContent: 'space-between'
  },
  restaurnatHeader: {
    paddingVertical: 5,
    lineHeight: 16,
    fontSize: 18,
    fontFamily: THEME.MEDIUM
  },
  restaurnatDescription: {},
  progress: {
    height: 48,
    justifyContent: 'center'
  }
})

OrderItem.propTypes = {
  item: PropTypes.objectOf(PropTypes.any).isRequired,
  isLast: PropTypes.bool.isRequired
}

export default OrderItem

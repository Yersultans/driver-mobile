import React from 'react'
import { gql, useMutation } from '@apollo/client'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { useAuth } from '../context/useAuth'
import Login from './Login'
import Loading from '../shared/Loading'

const LOGIN = gql`
  mutation login($input: LoginUserInput) {
    loginUser(input: $input) {
      token
    }
  }
`

const LoginContainer = () => {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')

  const [login, { client, data, error, loading }] = useMutation(LOGIN)
  const { fetchUser } = useAuth()
  const [isError, setIsError] = React.useState(false)
  async function setAsyncStorage(token) {
    await AsyncStorage.setItem('token', `${token}`)
    fetchUser()
  }

  React.useEffect(() => {
    if (!loading && error) {
      setIsError(true)
    } else if (data && data.loginUser && data.loginUser.token) {
      setAsyncStorage(`${data.loginUser.token}`)
      client.resetStore().then(() => {
        console.log('store has been reset')
        fetchUser()
      })
    }
  }, [data, loading, error])

  const handleSubmit = values => {
    login({
      variables: {
        input: values
      },
      errorPolicy: 'all'
    })
  }

  if (loading || data) {
    return <Loading />
  }

  return (
    <Login
      onSubmit={handleSubmit}
      isError={isError}
      username={username}
      setUsername={setUsername}
      password={password}
      setPassword={setPassword}
    />
  )
}

export default LoginContainer

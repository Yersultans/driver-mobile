import React from 'react'
import { View, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native'
import PropTypes from 'prop-types'

import THEME from '../shared/theme'
import HeaderNav from '../shared/HeaderNav'
import RegisterFormItem from '../shared/RegisterForm/RegisterFormItem'
import ValidationError from '../shared/RegisterForm/ValidationError'
import StatusBarCustom from '../shared/StatusBarCustom'
import Button from '../shared/Button'

const Login = ({
  onSubmit,
  isError,
  username,
  setUsername,
  password,
  setPassword
}) => {
  const emailRef = React.useRef()
  const passwordRef = React.useRef()

  const handleSubmit = () => {
    if (username && password) {
      onSubmit({ username, password })
    }
  }

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <HeaderNav
        style={styles.headerNav}
        title="Driver"
        withoutBackButton
        withNextBtn
        nextBtnColor={
          username && password ? THEME.COLOR_MAIN : THEME.COLOR_BLACK_OPACITY_4
        }
        onPressNext={handleSubmit}
      />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' && 'padding'}
      >
        <View
          style={{
            ...styles.container,
            marginTop: isError ? 0 : 32
          }}
        >
          {isError && <ValidationError error="Incorrect login or password" />}
          <View style={styles.inputsContainer}>
            <RegisterFormItem
              title="Email"
              autoCapitalize="none"
              placeholder="Email"
              reference={emailRef}
              value={username}
              onPress={() => emailRef.current.focus()}
              onChangeText={text => setUsername(text.trim().toLowerCase())}
            />
            <RegisterFormItem
              title="Password"
              placeholder="Password"
              isPassword
              reference={passwordRef}
              value={password}
              onPress={() => passwordRef.current.focus()}
              onChangeText={text => setPassword(text)}
            />
          </View>
        </View>
        <View style={styles.button}>
          <Button
            onPress={handleSubmit}
            paddingVertical={24}
            backgroundColor={
              username && password
                ? THEME.COLOR_MAIN
                : THEME.COLOR_BLACK_OPACITY_4
            }
            disabled={!(username && password)}
          >
            Sign In
          </Button>
        </View>
      </KeyboardAvoidingView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputsContainer: {
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  forgot: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 40
  },
  inner: {
    marginBottom: 40
  },
  eula: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 32
  },
  warning: {
    marginHorizontal: 24,
    textAlign: 'center',
    marginBottom: 6
  },
  button: {
    justifyContent: 'flex-end',
    paddingBottom: 40,
    paddingHorizontal: 24
  }
})

Login.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isError: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
  setUsername: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
  setPassword: PropTypes.func.isRequired
}

export default Login

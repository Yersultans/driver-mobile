import React from 'react'
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  Alert,
  Linking
} from 'react-native'
import MapView from 'react-native-maps'
import { gql, useMutation } from '@apollo/client'
import MapViewDirections from 'react-native-maps-directions'
import {
  Ionicons,
  Entypo,
  FontAwesome,
  SimpleLineIcons
} from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'

import StatusBarCustom from '../shared/StatusBarCustom'
import THEME from '../shared/theme'
import { useAuth } from '../context/useAuth'

const { Marker } = MapView
const GOOGLE_MAPS_APIKEY = 'AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4'
const mapRegion = {
  latitude: 43.238949,
  longitude: 76.889709,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
}

const DELETE_ORDER = gql`
  mutation driverCancelOrder($orderId: ID) {
    driverCancelOrder(orderId: $orderId) {
      id
    }
  }
`

const START_TRIP = gql`
  mutation startTrip($orderId: ID) {
    startTrip(orderId: $orderId) {
      id
    }
  }
`

const FINISH_TRIP = gql`
  mutation finishOrder($orderId: ID) {
    finishOrder(orderId: $orderId) {
      id
    }
  }
`

const WaitDriverMapContainer = ({ route }) => {
  const navigation = useNavigation()
  const {
    driverActiveOrder,
    dataSub,
    errorSub,
    loadingSub,
    refetchActiveOrder
  } = useAuth()

  const [deletelOrder] = useMutation(DELETE_ORDER, {
    onCompleted() {
      refetchActiveOrder()
      navigation.navigate('Map')
    }
  })

  const [startTrip] = useMutation(START_TRIP, {
    onCompleted() {
      refetchActiveOrder()
    }
  })

  const [finishedTrip] = useMutation(FINISH_TRIP, {
    onCompleted() {
      refetchActiveOrder()
      navigation.navigate('Map')
    }
  })

  React.useEffect(() => {
    if (dataSub && dataSub.order) {
      if (dataSub?.order?.data?.id === driverActiveOrder?.id) {
        refetchActiveOrder()
        if (dataSub?.order?.data?.status === 'finished') {
          navigation.navigate('Map')
        }
      }
    }
  }, [dataSub, errorSub, loadingSub])

  const handleCancel = () => {
    Alert.alert('Warning', 'You want to cancel the order', [
      {
        text: 'No',
        style: 'cancel'
      },
      { text: 'Yes', onPress: () => onCancel() }
    ])
  }

  const onCancel = () => {
    deletelOrder({
      variables: {
        orderId: driverActiveOrder?.id
      }
    })
  }

  const onStartTrip = () => {
    Alert.alert('Warning', 'Do you want to start your trip', [
      {
        text: 'No',
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () =>
          startTrip({
            variables: {
              orderId: driverActiveOrder?.id
            }
          })
      }
    ])
  }

  const onFinishedTip = () => {
    Alert.alert('Warning', 'Do you want to start your trip', [
      {
        text: 'No',
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () =>
          finishedTrip({
            variables: {
              orderId: driverActiveOrder?.id
            }
          })
      }
    ])
  }

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <MapView
        style={styles.map}
        showsUserLocation
        showsMyLocationButton
        initialRegion={mapRegion}
      >
        {driverActiveOrder?.formAddress && driverActiveOrder?.toAddress && (
          <MapViewDirections
            origin={{
              latitude: driverActiveOrder?.formAddress?.latitude,
              longitude: driverActiveOrder?.formAddress?.longitude
            }}
            destination={{
              latitude: driverActiveOrder?.toAddress?.latitude,
              longitude: driverActiveOrder?.toAddress?.longitude
            }}
            lineDashPattern={[1]}
            apikey={GOOGLE_MAPS_APIKEY} // insert your API Key here
            strokeWidth={4}
            strokeColor="green"
          />
        )}
        {driverActiveOrder?.formAddress && (
          <Marker
            coordinate={{
              latitude: driverActiveOrder?.formAddress?.latitude,
              longitude: driverActiveOrder?.formAddress?.longitude
            }}
            pinColor={THEME.COLOR_GOLD}
          />
        )}
        {driverActiveOrder?.toAddress && (
          <Marker
            coordinate={{
              latitude: driverActiveOrder?.toAddress?.latitude,
              longitude: driverActiveOrder?.toAddress?.longitude
            }}
            pinColor="red"
          />
        )}
      </MapView>
      {driverActiveOrder?.status && (
        <View
          style={{
            backgroundColor: '#E1E1E1',
            borderTopLeftRadius: 24,
            borderTopRightRadius: 24,
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0
          }}
        >
          <View
            style={{
              borderRadius: 24,
              backgroundColor: THEME.COLOR_WHITE,
              paddingHorizontal: 16,
              marginBottom: 8
            }}
          >
            <View
              style={{
                marginVertical: 12,
                paddingVertical: 8,
                borderBottomColor: '#E1E1E1',
                borderBottomWidth: 1
              }}
            >
              <Text
                style={{
                  lineHeight: 24,
                  fontSize: 24,
                  fontFamily: THEME.MEDIUM
                }}
              >
                Waiting for arrival
              </Text>
            </View>
            <View
              style={{
                paddingVertical: 24,
                flexDirection: 'row',
                justifyContent: 'center'
              }}
            >
              <View
                style={{
                  width: 96
                  // marginHorizontal: 16
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('DetailsOrder', {
                      order: driverActiveOrder
                    })
                  }}
                  style={{
                    borderRadius: 200,
                    backgroundColor: '#E1E1E1',
                    padding: 12,
                    alignSelf: 'center'
                  }}
                >
                  <Ionicons name="list" size={32} color="black" />
                </TouchableOpacity>
                <Text
                  style={{
                    lineHeight: 24,
                    fontSize: 16,
                    fontFamily: THEME.MEDIUM,
                    paddingLeft: 8,
                    textAlign: 'center'
                  }}
                >
                  Details
                </Text>
              </View>

              <View
                style={{
                  width: 96
                  // marginHorizontal: 16
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('Messages', {
                      order: driverActiveOrder
                    })
                  }}
                  style={{
                    borderRadius: 200,
                    backgroundColor: '#E1E1E1',
                    padding: 12,
                    alignSelf: 'center'
                  }}
                >
                  <FontAwesome name="comment" size={32} color="black" />
                </TouchableOpacity>
                <Text
                  style={{
                    lineHeight: 24,
                    fontSize: 16,
                    fontFamily: THEME.MEDIUM,
                    paddingLeft: 8,
                    textAlign: 'center'
                  }}
                >
                  Message
                </Text>
              </View>

              <View
                style={{
                  width: 96
                  // marginHorizontal: 16
                }}
              >
                <TouchableOpacity
                  onPress={async () => {
                    if (driverActiveOrder?.client?.phoneNumber) {
                      const supported = await Linking.canOpenURL(
                        `tel:${driverActiveOrder?.client?.phoneNumber}`
                      )

                      if (supported) {
                        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
                        // by some browser in the mobile
                        await Linking.openURL(
                          `tel:${driverActiveOrder?.client?.phoneNumber}`
                        )
                      }
                    } else {
                      const supported = await Linking.canOpenURL(`tel:`)

                      if (supported) {
                        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
                        // by some browser in the mobile
                        await Linking.openURL(`tel:`)
                      }
                    }
                  }}
                  style={{
                    borderRadius: 200,
                    backgroundColor: '#E1E1E1',
                    padding: 12,
                    alignSelf: 'center'
                  }}
                >
                  <Entypo name="phone" size={32} color="black" />
                </TouchableOpacity>
                <Text
                  style={{
                    lineHeight: 24,
                    fontSize: 16,
                    fontFamily: THEME.MEDIUM,
                    paddingLeft: 8,
                    textAlign: 'center'
                  }}
                >
                  Phone
                </Text>
              </View>
              {driverActiveOrder.status === 'findedDriver' && (
                <View
                  style={{
                    width: 96
                    // marginHorizontal: 16
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      handleCancel()
                    }}
                    style={{
                      borderRadius: 200,
                      backgroundColor: '#E1E1E1',
                      padding: 12,
                      alignSelf: 'center'
                    }}
                  >
                    <Ionicons name="close" size={32} color="black" />
                  </TouchableOpacity>
                  <Text
                    style={{
                      lineHeight: 24,
                      fontSize: 16,
                      fontFamily: THEME.MEDIUM,
                      paddingLeft: 8,
                      textAlign: 'center'
                    }}
                  >
                    Cancel
                  </Text>
                </View>
              )}
            </View>
            <TouchableOpacity
              onPress={() => {
                if (driverActiveOrder.status === 'findedDriver') {
                  onStartTrip()
                } else if (driverActiveOrder.status === 'findedDriver') {
                  onFinishedTip()
                }
              }}
              activeOpacity={0.8}
              underlayColor="transparent"
            >
              <View style={styles.button}>
                <View>
                  <Text style={styles.buttonText}>
                    {driverActiveOrder.status === 'findedDriver'
                      ? `I'm here`
                      : 'Start'}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )}
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Profile')
        }}
        style={{
          flex: 1,
          position: 'absolute',
          paddingTop: 8,
          paddingLeft: 8,
          left: 0,
          right: 0,
          top: 0
        }}
      >
        <SimpleLineIcons
          name="menu"
          size={24}
          color={THEME.COLOR_BLACK_OPACITY_5}
        />
      </TouchableOpacity>
    </>
  )
}

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  modalConatiner: {
    backgroundColor: THEME.COLOR_WHITE,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 16,
    lineHeight: 20
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  modal: {
    backgroundColor: '#E1E1E1',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24
  },
  content: {
    borderRadius: 24,
    backgroundColor: THEME.COLOR_WHITE,
    paddingVertical: 24,
    paddingHorizontal: 16,
    marginBottom: 8
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  }
})

export default WaitDriverMapContainer

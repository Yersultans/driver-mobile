import React from 'react'
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity
} from 'react-native'
import MapView from 'react-native-maps'
import { gql, useMutation, useLazyQuery, useSubscription } from '@apollo/client'
import { AntDesign, FontAwesome5, SimpleLineIcons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'

import StatusBarCustom from '../shared/StatusBarCustom'
import OrderInfoModal from './OrderInfoModal'
import THEME from '../shared/theme'
import { useAuth } from '../context/useAuth'

const { Marker } = MapView
const mapRegion = {
  latitude: 43.238949,
  longitude: 76.889709,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
}

const GET_ORDER_WITHOUT_DRIVER = gql`
  query ordersByStatus($status: String) {
    ordersByStatus(status: $status) {
      id
      formAddress {
        street
        latitude
        longitude
      }
      toAddress {
        street
        latitude
        longitude
      }
      driver {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      client {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      status
      price
      paymentMethod
    }
  }
`

const ADD_DRIVER_TO_ORDER = gql`
  mutation addDriverToOrder($orderId: ID, $userId: ID) {
    addDriverToOrder(orderId: $orderId, userId: $userId) {
      id
      formAddress {
        street
        latitude
        longitude
      }
      toAddress {
        street
        latitude
        longitude
      }
      driver {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      client {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      status
      price
      paymentMethod
    }
  }
`

const MapContainer = () => {
  const navigation = useNavigation()
  const {
    activeOrder,
    user,
    dataSub,
    errorSub,
    loadingSub,
    driverActiveOrder
  } = useAuth()
  const [orders, setOrders] = React.useState([])
  const [startMarker, setStartMarker] = React.useState(null)
  const [startMarkerName, setStartMarkerName] = React.useState('')
  const [selectOrder, setSelectOrder] = React.useState(null)

  const [getOrders, { data, loading, error, refetch }] = useLazyQuery(
    GET_ORDER_WITHOUT_DRIVER,
    {
      fetchPolicy: 'no-cache'
    }
  )

  const [addDriverToOrder] = useMutation(ADD_DRIVER_TO_ORDER, {
    onCompleted() {
      navigation.navigate('DriverMap')
    }
  })

  React.useEffect(() => {
    if (user?.role !== 'driver' && activeOrder) {
      navigation.navigate('WaitDriverMap')
    }
  }, [activeOrder])

  React.useEffect(() => {
    if (user?.role === 'driver' && driverActiveOrder) {
      console.log('user', user?.id)
      navigation.navigate('DriverMap')
    }
  }, [driverActiveOrder])

  React.useEffect(() => {
    if (user?.role === 'driver') {
      getOrders({
        variables: {
          status: 'findDriver'
        }
      })
    }
  }, [user])

  React.useEffect(() => {
    if (data && data.ordersByStatus && !loading) {
      setOrders(data.ordersByStatus)
    }
  }, [data, loading, error])

  React.useEffect(() => {
    if (dataSub && dataSub.order) {
      if (user?.role === 'driver') {
        refetch()
      }
    }
  }, [dataSub, errorSub, loadingSub])

  const takeOrder = ({ orderId }) => {
    addDriverToOrder({
      variables: {
        orderId,
        userId: user?.id
      }
    })
  }

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <MapView
        onPress={e => {
          if (user?.role !== 'driver') {
            setStartMarker(e.nativeEvent.coordinate)
            fetch(
              `https://maps.googleapis.com/maps/api/geocode/json?latlng=${e.nativeEvent.coordinate.latitude},${e.nativeEvent.coordinate.longitude}&key=AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4`
            )
              .then(response => response.json())
              .then(responseJson => {
                responseJson.results.forEach(result => {
                  if (result.types.includes('street_address')) {
                    setStartMarkerName(result.formatted_address)
                  }
                })
              })
          }
        }}
        style={styles.map}
        showsUserLocation
        showsMyLocationButton
        initialRegion={mapRegion}
      >
        {startMarker && (
          <Marker
            coordinate={{
              latitude: startMarker?.latitude,
              longitude: startMarker?.longitude
            }}
            pinColor={THEME.COLOR_GOLD}
          />
        )}
        {orders.map(order => {
          return (
            <Marker
              key={order?.id}
              coordinate={{
                latitude: order.formAddress.latitude,
                longitude: order.formAddress.longitude
              }}
              onPress={() => {
                setSelectOrder(order)
              }}
              pinColor={THEME.COLOR_GREEN}
            />
          )
        })}
      </MapView>

      {(activeOrder || driverActiveOrder) && (
        <TouchableOpacity
          onPress={() => {
            if (user?.role !== 'driver') {
              navigation.navigate('WaitDriverMap')
            } else if (user?.role === 'driver') {
              navigation.navigate('DriverMap')
            }
          }}
          style={{
            flex: 1,
            position: 'absolute',
            right: 0,
            top: 320,
            borderRadius: 200,
            backgroundColor: '#E1E1E1',
            paddingHorizontal: 10,
            paddingVertical: 10,
            marginHorizontal: 16
          }}
        >
          <FontAwesome5 name="car-side" size={24} color="black" />
        </TouchableOpacity>
      )}
      {startMarker && (
        <View
          style={{
            backgroundColor: '#E1E1E1',
            borderTopLeftRadius: 24,
            borderTopRightRadius: 24,
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0
          }}
        >
          <View
            style={{
              borderRadius: 24,
              backgroundColor: THEME.COLOR_WHITE,
              paddingVertical: 24,
              paddingHorizontal: 16,
              marginBottom: 8
            }}
          >
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('SecondMarker', {
                  startMarker,
                  startMarkerName,
                  isMarked: true
                })
              }}
              style={{
                borderRadius: 10,
                backgroundColor: '#E1E1E1',
                padding: 16,
                flexDirection: 'row',
                justifyContent: 'space-between'
              }}
            >
              <View
                style={{
                  flexDirection: 'row'
                }}
              >
                <FontAwesome5 name="car-side" size={24} color="black" />
                <Text
                  style={{
                    lineHeight: 24,
                    fontSize: 20,
                    fontFamily: THEME.MEDIUM,
                    textAlign: 'center',
                    paddingLeft: 8
                  }}
                >
                  Where we go?
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  borderLeftWidth: 1,
                  paddingLeft: 8
                }}
              >
                <AntDesign name="arrowright" size={24} color="black" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )}
      {selectOrder && (
        <OrderInfoModal
          onpen={!!selectOrder}
          onClose={() => setSelectOrder(null)}
          order={selectOrder}
          takeOrder={takeOrder}
        />
      )}
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Profile')
        }}
        style={{
          flex: 1,
          position: 'absolute',
          paddingTop: 8,
          paddingLeft: 8,
          left: 0,
          right: 0,
          top: 0
        }}
      >
        <SimpleLineIcons
          name="menu"
          size={24}
          color={THEME.COLOR_BLACK_OPACITY_5}
        />
      </TouchableOpacity>
    </>
  )
}

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  modalConatiner: {
    backgroundColor: THEME.COLOR_WHITE,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    marginTop: 8,
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 16,
    lineHeight: 20
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  modal: {
    backgroundColor: '#E1E1E1',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24
  },
  content: {
    borderRadius: 24,
    backgroundColor: THEME.COLOR_WHITE,
    paddingVertical: 24,
    paddingHorizontal: 16,
    marginBottom: 8
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  }
})

export default MapContainer

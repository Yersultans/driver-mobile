import React from 'react'
import { Modal, View, TouchableOpacity, StyleSheet, Text } from 'react-native'
import { FontAwesome5, AntDesign } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'
import THEME from '../shared/theme'
import Button from '../shared/Button'

const PaymentModal = ({ open, onClose }) => {
  const [values, setValues] = React.useState(null)
  const [isValid, setIsValid] = React.useState(false)
  const navigation = useNavigation()
  return (
    <Modal
      visible={open}
      transparent
      animationType="fade"
      onRequestClose={() => {
        onClose()
        setValues(null)
      }}
    >
      <TouchableOpacity style={styles.container} onPress={() => onClose()}>
        <View style={styles.modal}>
          <View style={styles.content}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('SecondMarker')
              }}
              style={{
                borderRadius: 10,
                backgroundColor: '#E1E1E1',
                padding: 16,
                flexDirection: 'row',
                justifyContent: 'space-between'
              }}
            >
              <View
                style={{
                  flexDirection: 'row'
                }}
              >
                <FontAwesome5 name="car-side" size={24} color="black" />
                <Text
                  style={{
                    lineHeight: 24,
                    fontSize: 20,
                    fontFamily: THEME.MEDIUM,
                    textAlign: 'center',
                    paddingLeft: 8
                  }}
                >
                  Where we go?
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  borderLeftWidth: 1,
                  paddingLeft: 8
                }}
              >
                <AntDesign name="arrowright" size={24} color="black" />
              </View>
            </TouchableOpacity>
          </View>
          {/* <View
            style={{
              borderRadius: 24,
              backgroundColor: THEME.COLOR_WHITE,
              paddingVertical: 24
            }}
          >
            <Text>asd</Text>
          </View> */}
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
    // backgroundColor: THEME.COLOR_BLACK_OPACITY_5
  },
  modal: {
    backgroundColor: '#E1E1E1',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24
  },
  content: {
    borderRadius: 24,
    backgroundColor: THEME.COLOR_WHITE,
    paddingVertical: 24,
    paddingHorizontal: 16,
    marginBottom: 8
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  }
})

PaymentModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
}

export default PaymentModal

import React from 'react'
import {
  Modal,
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
  Alert
} from 'react-native'
import { FontAwesome, AntDesign, Ionicons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'
import THEME from '../shared/theme'
import Button from '../shared/Button'

const OrdarInfoModal = ({ open, onClose, order, takeOrder }) => {
  const navigation = useNavigation()

  const handleTakeOrder = () => {
    Alert.alert('Warning', 'You go take this order', [
      {
        text: 'No',
        style: 'cancel'
      },
      { text: 'Yes', onPress: () => takeOrder({ orderId: order?.id }) }
    ])
  }

  return (
    <Modal
      visible={open}
      transparent
      animationType="fade"
      onRequestClose={() => {
        onClose()
      }}
    >
      <TouchableOpacity style={styles.container} onPress={() => onClose()}>
        <View
          style={{
            backgroundColor: THEME.COLOR_WHITE,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
            paddingVertical: 16,
            paddingHorizontal: 16
          }}
        >
          <View
            style={{
              width: '95%',
              paddingVertical: 16,
              flexDirection: 'row',
              alignItems: 'center',
              borderBottomWidth: 0.5,
              borderBottomColor: THEME.COLOR_BLACK_OPACITY_5
            }}
          >
            <FontAwesome
              name="dot-circle-o"
              size={16}
              color={THEME.COLOR_GOLD}
              style={{
                marginRight: 8
              }}
            />
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 16,
                lineHeight: 16
              }}
            >
              {order?.formAddress?.street}
            </Text>
          </View>
          <View
            style={{
              width: '95%',
              paddingVertical: 16,
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <FontAwesome
              name="dot-circle-o"
              size={16}
              color="#FF5C5C"
              style={{
                marginRight: 8
              }}
            />
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 16,
                lineHeight: 16
              }}
            >
              {order?.toAddress?.street}
            </Text>
          </View>
          <View
            style={{
              paddingVertical: 24,
              flexDirection: 'row',
              justifyContent: 'center'
            }}
          >
            <View
              style={{
                width: 96
                // marginHorizontal: 16
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('DetailsOrder', { order })
                }}
                style={{
                  borderRadius: 200,
                  backgroundColor: '#E1E1E1',
                  padding: 12,
                  alignSelf: 'center'
                }}
              >
                <Ionicons name="list" size={32} color="black" />
              </TouchableOpacity>
              <Text
                style={{
                  lineHeight: 24,
                  fontSize: 16,
                  fontFamily: THEME.MEDIUM,
                  paddingLeft: 8,
                  textAlign: 'center'
                }}
              >
                Details
              </Text>
            </View>
            <View
              style={{
                width: 96
                // marginHorizontal: 16
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  onClose()
                }}
                style={{
                  borderRadius: 200,
                  backgroundColor: '#E1E1E1',
                  padding: 12,
                  alignSelf: 'center'
                }}
              >
                <Ionicons name="close" size={32} color="black" />
              </TouchableOpacity>
              <Text
                style={{
                  lineHeight: 24,
                  fontSize: 16,
                  fontFamily: THEME.MEDIUM,
                  paddingLeft: 8,
                  textAlign: 'center'
                }}
              >
                Cancel
              </Text>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              handleTakeOrder()
            }}
            activeOpacity={0.8}
            underlayColor="transparent"
          >
            <View style={styles.button}>
              <View>
                <Text style={styles.buttonText}>Take order</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    backgroundColor: THEME.COLOR_BLACK_OPACITY_5
  },
  modal: {
    backgroundColor: '#E1E1E1',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24
  },
  content: {
    borderRadius: 24,
    backgroundColor: THEME.COLOR_WHITE,
    paddingVertical: 24,
    paddingHorizontal: 16,
    marginBottom: 8
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    marginTop: 8,
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 16,
    lineHeight: 20
  }
})

OrdarInfoModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
}

export default OrdarInfoModal

import React from 'react'
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity
} from 'react-native'
import MapView from 'react-native-maps'
import { gql, useQuery } from '@apollo/client'
import MapViewDirections from 'react-native-maps-directions'
import { FontAwesome, AntDesign } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'

import StatusBarCustom from '../shared/StatusBarCustom'
import THEME from '../shared/theme'
import RegisterFormItem from '../shared/RegisterForm/RegisterFormItem'
import { useAuth } from '../context/useAuth'

const { Marker } = MapView
const GOOGLE_MAPS_APIKEY = 'AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4'
const mapRegion = {
  latitude: 43.238949,
  longitude: 76.889709,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
}

const GET_CARDS = gql`
  query userCards($userId: ID) {
    userCards(userId: $userId) {
      id
      name
      expDate
      cvv
      type
      number
      amount
    }
  }
`

const ScondMarkContainer = ({ route }) => {
  const { startMarker, startMarkerName, isMarked } = route.params
  const navigation = useNavigation()
  const { user } = useAuth()
  const { data, loading, error, refetch } = useQuery(GET_CARDS, {
    fetchPolicy: 'no-cache',
    variables: {
      userId: user?.id
    }
  })
  const paymentMethodRef = React.useRef()
  const [paymentMethod, setPaymentMethod] = React.useState('')
  const priceRef = React.useRef()
  const [price, setPrice] = React.useState('')
  const [paymentMethods, setPaymentMethods] = React.useState([
    {
      label: 'Сash',
      value: 'cash'
    }
  ])
  const [isMarkedSecond, setIsMarkedSecond] = React.useState(false)
  const [endMarker, setEndMarker] = React.useState(null)
  const [endMarkerName, setEndMarkerName] = React.useState('')

  React.useEffect(() => {
    if (data && !loading) {
      setPaymentMethods(
        paymentMethods.concat(
          data.userCards.map(userCard => {
            const splitNumber = userCard?.number?.split(' ')
            return {
              label: `${userCard.type === 'visa' ? 'Visa' : 'MasterCard'} ${
                splitNumber[0]
              } **** **** ${splitNumber[3]}`,
              value: userCard.id
            }
          })
        )
      )
    }
  }, [data, loading, error])

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <MapView
        onPress={e => {
          if (startMarker && isMarked) {
            setEndMarker(e.nativeEvent.coordinate)
            fetch(
              `https://maps.googleapis.com/maps/api/geocode/json?latlng=${e.nativeEvent.coordinate.latitude},${e.nativeEvent.coordinate.longitude}&key=AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4`
            )
              .then(response => response.json())
              .then(responseJson => {
                responseJson.results.forEach(result => {
                  if (result.types.includes('street_address')) {
                    setEndMarkerName(result.formatted_address)
                  }
                })
              })
          }
        }}
        style={styles.map}
        showsUserLocation
        showsMyLocationButton
        initialRegion={mapRegion}
      >
        {/* {location && (
          <Marker
            coordinate={{
              latitude: location?.latitude,
              longitude: location?.longitude
            }}
          />
        )} */}
        {startMarker && endMarker && (
          <MapViewDirections
            origin={startMarker}
            destination={endMarker}
            lineDashPattern={[1]}
            apikey={GOOGLE_MAPS_APIKEY} // insert your API Key here
            strokeWidth={4}
            strokeColor="green"
          />
        )}
        {startMarker && (
          <Marker
            coordinate={{
              latitude: startMarker?.latitude,
              longitude: startMarker?.longitude
            }}
            pinColor={THEME.COLOR_GOLD}
          />
        )}
        {endMarker && (
          <Marker
            coordinate={{
              latitude: endMarker?.latitude,
              longitude: endMarker?.longitude
            }}
            pinColor="red"
          />
        )}
      </MapView>
      {startMarker && endMarker && (
        <View style={styles.modalConatiner}>
          <View
            style={{
              paddingVertical: 16,
              borderBottomColor: THEME.COLOR_BLACK_OPACITY_5,
              borderBottomWidth: 0.5
            }}
          >
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 20,
                lineHeight: 20
              }}
            >
              Destination point
            </Text>
          </View>
          <View
            style={{
              width: '95%',
              paddingVertical: 16,
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <FontAwesome
              name="dot-circle-o"
              size={16}
              color={THEME.COLOR_GOLD}
              style={{
                marginRight: 8
              }}
            />
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 16,
                lineHeight: 16
              }}
            >
              {endMarkerName}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('FinishedMarker', {
                startMarker,
                startMarkerName,
                isMarked: true,
                endMarker,
                endMarkerName,
                isMarkedSecond: true
              })
            }}
            activeOpacity={0.8}
            underlayColor="transparent"
          >
            <View style={styles.button}>
              <View>
                <Text style={styles.buttonText}>Ready</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )}
      {startMarker && (
        <View
          style={{
            flex: 1,
            position: 'absolute',
            paddingTop: 8,
            paddingLeft: 8,
            left: 0,
            right: 0,
            top: 0,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}
          >
            <AntDesign
              name="leftcircle"
              size={32}
              color={THEME.COLOR_BLACK_OPACITY_5}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              lineHeight: 24,
              fontFamily: THEME.MEDIUM
            }}
          >
            Mark places
          </Text>
          <View
            style={{
              width: 50,
              height: 30
            }}
          />
        </View>
      )}
      {startMarker && endMarker && isMarkedSecond && (
        <View style={styles.modalConatiner}>
          <View
            style={{
              width: '95%',
              paddingVertical: 16,
              flexDirection: 'row',
              alignItems: 'center',
              borderBottomWidth: 0.5,
              borderBottomColor: THEME.COLOR_BLACK_OPACITY_5
            }}
          >
            <FontAwesome
              name="dot-circle-o"
              size={16}
              color={THEME.COLOR_GOLD}
              style={{
                marginRight: 8
              }}
            />
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 16,
                lineHeight: 16
              }}
            >
              {startMarkerName}
            </Text>
          </View>
          <View
            style={{
              width: '95%',
              paddingVertical: 16,
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <FontAwesome
              name="dot-circle-o"
              size={16}
              color={THEME.COLOR_GOLD}
              style={{
                marginRight: 8
              }}
            />
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 16,
                lineHeight: 16
              }}
            >
              {endMarkerName}
            </Text>
          </View>
          <View>
            <RegisterFormItem
              title="Price"
              autoCapitalize="none"
              placeholder="Price"
              reference={priceRef}
              value={price}
              onPress={() => priceRef.current.focus()}
              onChangeText={text => setPrice(text.trim().toLowerCase())}
            />
          </View>
          <View>
            <RegisterFormItem
              title="Payment method"
              reference={paymentMethodRef}
              onPress={() => paymentMethodRef.current.togglePicker()}
              withPicker
              onValueChange={value => {
                setPaymentMethod(value)
              }}
              items={paymentMethods}
              pickerPlaceholder={{
                label: 'Select Payment',
                value: ''
              }}
              value={paymentMethod}
            />
          </View>
          <TouchableOpacity
            onPress={() => {}}
            activeOpacity={0.8}
            underlayColor="transparent"
          >
            <View style={styles.button}>
              <View>
                <Text style={styles.buttonText}>Ready</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  modalConatiner: {
    backgroundColor: THEME.COLOR_WHITE,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    marginTop: 8,
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 16,
    lineHeight: 20
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  modal: {
    backgroundColor: '#E1E1E1',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24
  },
  content: {
    borderRadius: 24,
    backgroundColor: THEME.COLOR_WHITE,
    paddingVertical: 24,
    paddingHorizontal: 16,
    marginBottom: 8
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  }
})

export default ScondMarkContainer

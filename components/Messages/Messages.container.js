import React from 'react'
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableHighlight
} from 'react-native'
import { gql, useMutation, useQuery } from '@apollo/client'
import { AntDesign } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'

import PropTypes from 'prop-types'
import THEME from '../shared/theme'

import Loading from '../shared/Loading'
import { useAuth } from '../context/useAuth'

const MESSAGES = gql`
  query messagesByOrderId($orderId: ID) {
    messagesByOrderId(orderId: $orderId) {
      id
      text
      isReaded
      fromUser {
        id
        username
        lastname
        firstname
        phoneNumber
        avatarUrl
      }
      toUser {
        id
        username
        lastname
        firstname
        phoneNumber
        avatarUrl
      }
      order {
        id
      }
      created_at
    }
  }
`

const READ_ALL_MESSAGES = gql`
  mutation readAllMessages($userId: ID) {
    readAllMessages(userId: $userId) {
      message
      status
    }
  }
`

const ADD_MESSAGE = gql`
  mutation addUserMessage($input: UserMessageInput) {
    addUserMessage(input: $input) {
      id
    }
  }
`

const MessagesConatiner = ({ route }) => {
  const { order } = route.params
  const orderId = order?.id
  const { user, dataSubMessage, errorSubMessage, loadingSubMessage } = useAuth()
  const navigation = useNavigation()
  const [text, setText] = React.useState(null)
  const [messages, setMessages] = React.useState([])
  const textRef = React.useRef()

  const { data, loading, error, refetch } = useQuery(MESSAGES, {
    fetchPolicy: 'no-cache',
    variables: { orderId }
  })

  const [addMessage] = useMutation(ADD_MESSAGE, {
    onCompleted() {
      refetch()
    }
  })
  const [readAllMessages] = useMutation(READ_ALL_MESSAGES, {
    onCompleted() {
      refetch()
    }
  })

  React.useEffect(() => {
    if (user) {
      readAllMessages({
        variables: {
          userId: user?.id
        }
      })
    }
  }, [])

  React.useEffect(() => {
    if (data && !loading) {
      setMessages(data.messagesByOrderId.reverse())
    }
  }, [data, loading, error])

  React.useEffect(() => {
    if (dataSubMessage) {
      if (dataSubMessage?.userMessage?.data?.order?.id === orderId) {
        refetch()
        readAllMessages({
          variables: {
            userId: user?.id
          }
        })
      }
    }
  }, [dataSubMessage, errorSubMessage, loadingSubMessage])

  const handleSubmit = () => {
    setText(null)
    addMessage({
      variables: {
        input: {
          text,
          fromUser: user?.id,
          toUser:
            user?.id === order?.driver?.id
              ? order?.client?.id
              : order?.driver?.id,
          order: orderId
        }
      }
    })
  }

  return (
    <>
      <View
        style={{
          backgroundColor: '#fff',
          height: '100%'
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingVertical: 16,
            paddingHorizontal: 20,
            borderBottomColor: THEME.COLOR_BLACK_OPACITY_2,
            borderBottomWidth: 1
          }}
        >
          <Text
            style={{
              fontSize: 18,
              lineHeight: 24,
              fontFamily: THEME.MEDIUM
            }}
          >
            Messages
          </Text>
          <AntDesign
            name="close"
            size={24}
            color="black"
            onPress={() => navigation.goBack()}
          />
        </View>
        <ScrollView>
          {/* {loading ? (
            <Loading />
          ) : (
            <> */}
          {messages &&
            messages.map((message, index) => (
              <View
                key={message?.id}
                style={{
                  marginHorizontal: 20,
                  marginTop: 20,
                  flexDirection: 'row',
                  padding: 16,
                  borderWidth: 1,
                  borderColor: THEME.COLOR_BLACK_OPACITY_2,
                  borderRadius: 8,
                  marginBottom: index === messages.length - 1 ? 136 : 0
                }}
              >
                <View>
                  <Image
                    style={styles.image}
                    source={{
                      uri: message?.fromUser?.avatarUrl
                    }}
                  />
                </View>
                <View
                  style={{
                    marginLeft: 16
                  }}
                >
                  <View>
                    <Text
                      style={{
                        color: THEME.COLOR_BLACK_OPACITY_5,
                        fontFamily: THEME.REGULAR
                      }}
                    >
                      {message?.fromUser?.firstname}{' '}
                      {message?.fromUser?.lastname}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: THEME.COLOR_BLACK,
                        fontFamily: THEME.REGULAR,
                        fontSize: 16
                      }}
                    >
                      {message?.text}
                    </Text>
                  </View>
                </View>
              </View>
            ))}
          {/* )} */}
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            paddingVertical: 16,
            paddingHorizontal: 20
          }}
        >
          <TouchableOpacity
            style={{
              marginBottom: 8
            }}
            onPress={() => textRef.current.focus()}
          >
            <TextInput
              placeholder="Message"
              ref={textRef}
              style={{
                fontFamily: THEME.REGULAR,
                fontSize: 16,
                backgroundColor: THEME.COLOR_BLACK_OPACITY_05,
                borderRadius: 8,
                padding: 8
              }}
              onChangeText={txt => setText(txt)}
              value={text}
              autoCapitalize="none"
            />
          </TouchableOpacity>
          <TouchableHighlight
            onPress={() => {
              handleSubmit()
            }}
            activeOpacity={0.8}
            underlayColor="transparent"
          >
            <View style={styles.button}>
              <View>
                <Text style={styles.buttonText}>Send</Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
        <View />
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    backgroundColor: THEME.COLOR_BLACK_OPACITY_5
  },
  image: {
    height: 50,
    width: 50,
    borderRadius: 25
  },
  modal: {
    backgroundColor: THEME.COLOR_WHITE,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    paddingVertical: 16
  },
  firstBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  description: {
    color: THEME.COLOR_BLACK_OPACITY_5,
    fontFamily: THEME.REGULAR,
    fontSize: 16
  },
  text: {
    marginLeft: 16,
    color: THEME.COLOR_BLACK,
    fontFamily: THEME.MEDIUM,
    fontSize: 16
  },
  version: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_BLACK_OPACITY_4,
    paddingTop: 16,
    paddingBottom: 32,
    paddingLeft: 24
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    paddingHorizontal: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    alignItems: 'center',
    padding: 16,
    fontSize: 16,
    lineHeight: 20
  }
})

MessagesConatiner.propTypes = {}

export default MessagesConatiner

import React from 'react'
import { useNavigation } from '@react-navigation/native'

import { useAuth } from '../context/useAuth'
import HeaderNav from '../shared/HeaderNav'
import StatusBarCustom from '../shared/StatusBarCustom'
import THEME from '../shared/theme'
import Loading from '../shared/Loading'
import Profile from './Profile.design'

const ProfileContainer = () => {
  const { user } = useAuth()
  const navigation = useNavigation()

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <HeaderNav
        title="My Profile"
        backgroundColor={THEME.COLOR_WHITE}
        onPress={() => navigation.goBack()}
      />
      {user ? <Profile user={user} /> : <Loading />}
    </>
  )
}

export default ProfileContainer

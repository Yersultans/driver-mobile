import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Alert
} from 'react-native'
import PropTypes from 'prop-types'
import { useNavigation } from '@react-navigation/native'
import moment from 'moment'
import { FontAwesome, AntDesign } from '@expo/vector-icons'

import localization from 'moment/locale/ru'

import { useAuth } from '../context/useAuth'
import THEME from '../shared/theme'

const Profile = ({ user }) => {
  const { logout } = useAuth()
  const navigation = useNavigation()
  moment.updateLocale('ru', localization)
  const onLogOut = () => {
    Alert.alert('Warning', 'Do you want to log out of your account?', [
      {
        text: 'No',
        style: 'cancel'
      },
      { text: 'Yes', onPress: () => logout() }
    ])
  }
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerTitle}>Personal details</Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('UpdateUser', { userId: user.id })
          }}
        >
          <Text style={styles.change}>change</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.userContainer}>
        <View style={styles.userImage}>
          {user?.avatarUrl ? (
            <Image
              style={styles.image}
              source={{
                uri: user?.avatarUrl
              }}
            />
          ) : (
            <FontAwesome name="user-circle-o" size={128} color="black" />
          )}
        </View>
      </View>
      <View style={styles.userInfo}>
        <Text style={styles.userNameText}>
          {user.firstname} {user.lastname}
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('HistoryOrders', { userId: user?.id })
        }}
        style={styles.item}
      >
        <Text style={styles.itemText}>Orders</Text>
        <AntDesign name="right" size={24} color="black" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Cards', { userId: user.id })
        }}
        style={styles.item}
      >
        <Text style={styles.itemText}>Cards</Text>
        <AntDesign name="right" size={24} color="black" />
      </TouchableOpacity>
      <TouchableOpacity onPress={onLogOut} style={styles.item}>
        <Text style={styles.itemText}>Logout</Text>
        <AntDesign name="logout" size={24} color="black" />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 16,
    paddingHorizontal: 16,
    backgroundColor: THEME.COLOR_WHITE
  },
  headerContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  user: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  userImage: {},
  image: {
    width: 128,
    height: 128,
    borderRadius: 100
  },
  headerTitle: {
    fontFamily: THEME.MEDIUM,
    fontSize: 18,
    lineHeight: 21
  },
  change: {
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_MAIN,
    fontSize: 15,
    lineHeight: 18
  },
  btnContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: THEME.COLOR_GRAY_OPACITY_05,
    paddingVertical: 8,
    borderBottomWidth: 1
  },
  btnText: {
    fontFamily: THEME.REGULAR,
    fontSize: 16,
    lineHeight: 16
  },
  userContainer: {
    padding: 16,
    borderRadius: 20,
    backgroundColor: THEME.COLOR_WHITE,
    alignItems: 'center'
  },
  userInfo: {
    flex: 1,
    marginHorizontal: 16,
    alignItems: 'center'
  },
  userNameText: {
    fontFamily: THEME.MEDIUM,
    fontSize: 20,
    lineHeight: 20
  },
  otherInfo: {
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_5,
    borderBottomWidth: 0.5
  },
  otherInfoText: {
    fontFamily: THEME.REGULAR,
    fontSize: 15,
    lineHeight: 18,
    color: THEME.COLOR_BLACK_OPACITY_5,
    paddingVertical: 7
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    backgroundColor: THEME.COLOR_WHITE,
    borderWidth: 1,
    borderColor: THEME.COLOR_BLACK,
    marginTop: 16
  },
  itemText: {
    fontFamily: THEME.MEDIUM,
    fontSize: 18,
    lineHeight: 21
  }
})

Profile.propTypes = {
  user: PropTypes.shape().isRequired
}

export default Profile

import React from 'react'
import { gql, useMutation } from '@apollo/client'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { useAuth } from '../context/useAuth'
import Register from './Register'
import Loading from '../shared/Loading'

const REGISTER = gql`
  mutation register($input: RegisterUserInput) {
    register(input: $input) {
      token
    }
  }
`

const RegistrationContainer = () => {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [firstname, setFirstname] = React.useState('')
  const [lastname, setLastname] = React.useState('')
  const [phoneNumber, setPhoneNumber] = React.useState('')
  const [gender, setGender] = React.useState('')

  const [register, { client, data, error, loading }] = useMutation(REGISTER)
  const { fetchUser } = useAuth()
  const [isError, setIsError] = React.useState(false)
  async function setAsyncStorage(token) {
    await AsyncStorage.setItem('token', `${token}`)
    fetchUser()
  }

  React.useEffect(() => {
    if (!loading && error) {
      console.log('error', error)
      setIsError(true)
    } else if (data && data.register && data.register.token) {
      setAsyncStorage(`${data.register.token}`)
      client.resetStore().then(() => {
        fetchUser()
      })
    }
  }, [data, loading, error])

  const handleSubmit = values => {
    register({
      variables: {
        input: { ...values, role: 'user' }
      },
      errorPolicy: 'all'
    })
  }

  if (loading || data) {
    return <Loading />
  }

  return (
    <Register
      onSubmit={handleSubmit}
      isError={isError}
      username={username}
      setUsername={setUsername}
      password={password}
      setPassword={setPassword}
      firstname={firstname}
      setFirstname={setFirstname}
      lastname={lastname}
      setLastname={setLastname}
      phoneNumber={phoneNumber}
      setPhoneNumber={setPhoneNumber}
      gender={gender}
      setGender={setGender}
    />
  )
}

export default RegistrationContainer

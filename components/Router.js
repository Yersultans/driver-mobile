import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Feather, AntDesign } from '@expo/vector-icons'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { useAuth } from './context/useAuth'

import StartScreen from './StartScreen/StartScreen'
import WalkthroughtSreen from './StartScreen/WalkthroughtScreen'
import LoginContainer from './Login/Login.container'
import MapContainer from './Map/Map.container'
import ProfileContainer from './Profile/Profile.container'
import CardsContainer from './Cards/Cards.container'
import SecondMarkerContainer from './Map/SecondMarker.container'
import FinishedMarkerContainer from './Map/FinishedMarker.container'
import UpdateUserContainer from './UpdateUser/UpdateUser.container'
import WaitDriverMapContainer from './Map/WaitDriverMap.container'
import DetailsOrderContainer from './DetailsOrder/DetailsOrder.container'
import DriverMapContainer from './Map/DriverMap.container'
import HistoryOrdersContainer from './HistoryOrders/HistoryOrders.container'
import MessagesContainer from './Messages/Messages.container'
import RegisterContainer from './Register/Register.container'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const BottomTabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: 'black',
        inactiveTintColor: 'rgba(0, 0, 0, 0.4)',
        style: {
          borderTopWidth: 0,
          borderTopColor: 'rgba(0, 0, 0, 0.05)',
          backgroundColor: '#FFFFFF'
        }
      }}
    >
      <Tab.Screen
        name="Orders"
        component={MapContainer}
        options={{
          title: '',
          tabBarIcon: ({ color, size }) => (
            <View style={{ marginTop: 6 }}>
              <Feather name="map-pin" size={size} color={color} />
            </View>
          )
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileContainer}
        options={{
          title: '',
          tabBarIcon: ({ color, size }) => (
            <View style={{ marginTop: 6 }}>
              <AntDesign name="user" size={size} color={color} />
            </View>
          )
        }}
      />
    </Tab.Navigator>
  )
}
const Router = () => {
  const { checkUserIsLoggedIn } = useAuth()
  const [token, setToken] = React.useState('defualt')
  const navigationRef = React.useRef()

  React.useEffect(() => {
    checkUserIsLoggedIn().then(tokenData => {
      setToken(tokenData)
    })
  })

  return (
    <>
      {token !== 'defualt' && (
        <NavigationContainer independent ref={navigationRef}>
          <Stack.Navigator>
            {!token ? (
              <>
                <Stack.Screen
                  name="StartScreen"
                  component={StartScreen}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Login"
                  component={LoginContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Walkthrought"
                  component={WalkthroughtSreen}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Register"
                  component={RegisterContainer}
                  options={{ headerShown: false }}
                />
              </>
            ) : (
              <>
                <Stack.Screen
                  name="Map"
                  component={MapContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Profile"
                  component={ProfileContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Cards"
                  component={CardsContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="SecondMarker"
                  component={SecondMarkerContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="FinishedMarker"
                  component={FinishedMarkerContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="UpdateUser"
                  component={UpdateUserContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="WaitDriverMap"
                  component={WaitDriverMapContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="DetailsOrder"
                  component={DetailsOrderContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="DriverMap"
                  component={DriverMapContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="HistoryOrders"
                  component={HistoryOrdersContainer}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Messages"
                  component={MessagesContainer}
                  options={{ headerShown: false }}
                />
              </>
            )}
          </Stack.Navigator>
        </NavigationContainer>
      )}
    </>
  )
}

export default Router

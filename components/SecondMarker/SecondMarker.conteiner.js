import React from 'react'
import {
  StyleSheet,
  Dimensions,
  View,
  TouchableHighlight,
  Text
} from 'react-native'
import moment from 'moment'
import localization from 'moment/locale/ru'
import MapView, { Marker } from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import { FontAwesome } from '@expo/vector-icons'

import { useAuth } from '../context/useAuth'
import StatusBarCustom from '../shared/StatusBarCustom'
import THEME from '../shared/theme'

const MapContainer = () => {
  const [endMarkerName, setEndMarkerName] = React.useState('')
  const mapRegion = {
    latitude: 43.238949,
    longitude: 76.889709,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421
  }
  const GOOGLE_MAPS_APIKEY = 'AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4'
  const { startMarker } = useAuth()
  const [endMarker, setEndMarker] = React.useState(null)
  moment.updateLocale('ru', localization)

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <MapView
        onPress={e => {
          fetch(
            `https://maps.googleapis.com/maps/api/geocode/json?latlng=${e.nativeEvent.coordinate.latitude},${e.nativeEvent.coordinate.longitude}&key=AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4`
          )
            .then(response => response.json())
            .then(responseJson => {
              responseJson.results.forEach(result => {
                if (result.types.includes('street_address')) {
                  setEndMarkerName(result.formatted_address)
                }
              })
            })
          setEndMarker(e.nativeEvent.coordinate)
        }}
        style={styles.map}
        showsUserLocation
        showsMyLocationButton
        initialRegion={mapRegion}
      >
        {startMarker && endMarker && (
          <MapViewDirections
            origin={startMarker}
            destination={endMarker}
            lineDashPattern={[1]}
            apikey={GOOGLE_MAPS_APIKEY} // insert your API Key here
            strokeWidth={4}
            strokeColor="green"
          />
        )}
        {startMarker && (
          <Marker
            coordinate={{
              latitude: startMarker?.latitude,
              longitude: startMarker?.longitude
            }}
          />
        )}
        {endMarker && (
          <Marker
            coordinate={{
              latitude: endMarker?.latitude,
              longitude: endMarker?.longitude
            }}
          />
        )}
      </MapView>
      {startMarker && endMarker && (
        <View style={styles.modalConatiner}>
          <View
            style={{
              paddingVertical: 16,
              borderBottomColor: THEME.COLOR_BLACK_OPACITY_5,
              borderBottomWidth: 0.5
            }}
          >
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 20,
                lineHeight: 20
              }}
            >
              Destination point
            </Text>
          </View>
          <View
            style={{
              paddingVertical: 16,
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <FontAwesome
              name="dot-circle-o"
              size={16}
              color={THEME.COLOR_GOLD}
              style={{
                marginRight: 8
              }}
            />
            <Text
              style={{
                fontFamily: THEME.MEDIUM,
                color: THEME.COLOR_BLACK,
                fontSize: 16,
                lineHeight: 16
              }}
            >
              {endMarkerName}
            </Text>
          </View>
          <TouchableHighlight
            onPress={() => {
              navigate
            }}
            activeOpacity={0.8}
            underlayColor="transparent"
          >
            <View style={styles.button}>
              <View>
                <Text style={styles.buttonText}>Ready</Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME.COLOR_WHITE
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  modalConatiner: {
    backgroundColor: THEME.COLOR_WHITE,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 10,
    marginTop: 8,
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  buttonText: {
    fontFamily: THEME.MEDIUM,
    color: THEME.COLOR_WHITE,
    fontSize: 16,
    lineHeight: 20
  }
})

export default MapContainer

import React from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  Platform
} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import AsyncStorageNew from '@react-native-async-storage/async-storage'

import THEME from '../shared/theme'
import Button from '../shared/Button'
// import LogoIcon from '../../assets/svg/LogoIcon'
import StatusBarCustom from '../shared/StatusBarCustom'
import appJson from '../../app.json'

const { height } = Dimensions.get('window')
const { width } = Dimensions.get('window')

const logoImage = require('../../assets/icon.png')

const StartScreen = () => {
  const navigation = useNavigation()
  const checkForNotifications = async () => {
    try {
      const user = await AsyncStorageNew.getItem('user')
      if (!user) {
        AsyncStorageNew.setItem('user', 'true')
        console.log('new notification key was created')
      }
    } catch (e) {
      console.log('e', e)
    }
  }

  React.useEffect(() => {
    checkForNotifications()
  }, [])

  return (
    <View style={styles.container}>
      {/* <Image source={image} style={styles.img} /> */}
      {Platform.OS === 'ios' && (
        <StatusBarCustom
          backgroundColor="transparent"
          barStyle="light-content"
        />
      )}
      <View style={styles.inner}>
        {/* <View style={styles.logoContainer}>
          <Image source={logoImage} style={styles.logoContent} />
        </View> */}

        <Text style={styles.titleText}>Driver</Text>
        <Text style={styles.descriptionText}>
          Portal for logistics and search for transportation vehicles{' '}
        </Text>
      </View>
      <View style={styles.btnsContent}>
        <View style={styles.goBtnContainer}>
          <Button
            onPress={() => navigation.navigate('Walkthrought')}
            paddingVertical={height < 700 ? 18 : 24}
            backgroundColor={THEME.COLOR_MAIN}
            paddingHorizontal={24}
          >
            Go on
          </Button>
        </View>
        <View style={styles.goBtnContainer}>
          <Button
            onPress={() => navigation.navigate('Login')}
            paddingVertical={height < 700 ? 18 : 24}
            backgroundColor={THEME.COLOR_DISABLED_GRAY}
            color={THEME.COLOR_BLACK}
            paddingHorizontal={24}
          >
            Sign in
          </Button>
        </View>
      </View>

      <Text style={styles.version}>{`v ${appJson.expo.version}`}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#342359',
    justifyContent: 'flex-end'
  },
  inner: {
    height: '55%',
    width: '100%',
    bottom: height < 700 ? 32 : 96
  },
  img: {
    height: height < 700 ? '120%' : '100%',
    width: '100%',
    marginBottom: 'auto'
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoContent: {
    width: '80%',
    height: '80%'
  },
  titleText: {
    fontSize: height < 700 ? 24 : 40,
    lineHeight: height < 700 ? 34 : 48,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: THEME.COLOR_WHITE,
    fontFamily: THEME.SEMI_BOLD
  },
  descriptionText: {
    fontSize: height < 700 ? 16 : 20,
    lineHeight: height < 700 ? 18 : 24,
    textAlign: 'center',
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_WHITE,
    paddingHorizontal: width > 400 ? 24 : 12,
    marginBottom: height < 700 ? 32 : 32,
    marginTop: height < 700 ? 16 : 24
  },
  btnsContent: {
    marginVertical: height < 700 ? 24 : 32
  },
  goBtnContainer: {
    backgroundColor: THEME.COLOR_MAIN,
    marginHorizontal: height < 700 ? 20 : 24,
    marginBottom: height < 700 ? 8 : 12,
    borderRadius: 8
  },
  goBtnText: {
    color: THEME.COLOR_WHITE,
    fontFamily: THEME.MEDIUM,
    fontSize: 24,
    lineHeight: 24,
    paddingVertical: 24,
    textAlign: 'center'
  },
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    height: '100%'
  },
  version: {
    position: 'absolute',
    fontFamily: THEME.REGULAR,
    color: THEME.COLOR_WHITE_OPACITY_4,
    bottom: height < 700 ? 10 : 32,
    left: 24
  }
})

export default StartScreen

import React from 'react'
import { Text, View, StyleSheet, SafeAreaView, Dimensions } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import { useNavigation } from '@react-navigation/native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { FontAwesome5, Octicons, Entypo } from '@expo/vector-icons'

import THEME from '../shared/theme'
import HeaderNav from '../shared/HeaderNav'
// import TimerIcon from '../../assets/svg/WalkthroughtScreenSvg/TimerIcon'
// import CrownIcon from '../../assets/svg/WalkthroughtScreenSvg/CrownIcon'
// import BarchartIcon from '../../assets/svg/WalkthroughtScreenSvg/BarchartIcon'
import StatusBarCustom from '../shared/StatusBarCustom'

const { height } = Dimensions.get('window')

const WalkthroughtSreen = () => {
  const navigation = useNavigation()
  const skipRef = React.useRef()
  const slides = [
    {
      key: 's1',
      title: `Make an order`,
      text: 'Make an order from point A to point B',
      image: <Entypo name="location" size={240} color="black" />
    },
    {
      key: 's2',
      title: 'Announcement for drivers',
      text: 'Your ad will be shown to all available drivers',
      image: <FontAwesome5 name="list-ol" size={240} color="black" />
    },
    {
      key: 's3',
      title: 'End of Order',
      text:
        'After completing the order, you will need to confirm the end of the order',
      image: <Octicons name="checklist" size={240} color="black" />
    }
  ]

  const renderItem = ({ item }) => {
    return (
      <>
        <SafeAreaView style={styles.container}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.text}>{item.text}</Text>
          <View style={styles.imgContainer}>{item.image}</View>
          <View style={styles.empty} />
        </SafeAreaView>
      </>
    )
  }

  const onDone = () => {
    navigation.navigate('Register')
  }
  const renderNextButton = () => {
    return (
      <View style={styles.nextContainer}>
        <Text style={styles.nextText}>Next</Text>
      </View>
    )
  }

  const renderDoneButton = () => {
    return (
      <View style={styles.nextContainer}>
        <Text style={styles.nextText}>Next</Text>
      </View>
    )
  }

  const renderSkipButton = () => {
    return (
      <View style={styles.skipContainer}>
        <Text style={styles.skipText}>Skip</Text>
      </View>
    )
  }

  return (
    <>
      <StatusBarCustom />
      <HeaderNav
        onPress={() => navigation.goBack()}
        withNextBtn={height < 700}
        nextBtnTitle="Skip"
        onPressNext={() => skipRef.current.goToSlide(slides.length - 1)}
      />
      <AppIntroSlider
        data={slides}
        renderDoneButton={renderDoneButton}
        renderNextButton={renderNextButton}
        renderSkipButton={renderSkipButton}
        renderItem={renderItem}
        onDone={onDone}
        showSkipButton={height > 700}
        bottomButton
        dotStyle={styles.dotStyle}
        activeDotStyle={styles.activeDotStyle}
        ref={skipRef}
      />
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME.COLOR_WHITE,
    paddingHorizontal: 24
  },
  title: {
    fontFamily: THEME.MEDIUM,
    fontSize: height < 700 ? 20 : 28,
    lineHeight: 32,
    color: THEME.COLOR_BLACK,
    textAlign: 'center',
    marginTop: height < 700 ? 16 : 40 + getStatusBarHeight(),
    marginBottom: height < 700 ? 16 : 24
  },
  text: {
    fontFamily: THEME.REGULAR,
    fontSize: height < 700 ? 14 : 16,
    paddingHorizontal: 24,
    lineHeight: 24,
    color: THEME.COLOR_BLACK,
    textAlign: 'center'
  },
  imgContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  nextContainer: {
    backgroundColor: THEME.COLOR_MAIN,
    borderRadius: 8,
    paddingVertical: height < 700 ? 18 : 24,
    marginTop: height < 700 ? 4 : 28
  },
  nextText: {
    fontFamily: THEME.MEDIUM,
    fontSize: height < 700 ? 18 : 24,
    lineHeight: 24,
    alignItems: 'center',
    textAlign: 'center',
    color: THEME.COLOR_WHITE
  },
  skipContainer: {
    marginTop: 32,
    marginBottom: 64
  },
  skipText: {
    fontFamily: THEME.MEDIUM,
    fontSize: 20,
    lineHeight: 24,
    alignItems: 'center',
    textAlign: 'center',
    color: THEME.COLOR_BLACK_OPACITY_5
  },
  dotStyle: {
    backgroundColor: THEME.COLOR_BLACK_OPACITY_3,
    width: 8,
    height: 8,
    marginBottom: 0
  },
  activeDotStyle: {
    backgroundColor: THEME.COLOR_BLACK,
    width: 8,
    height: 8
  },
  empty: {
    height: height < 700 ? 112 : 248,
    marginTop: 'auto'
  }
})

export default WalkthroughtSreen

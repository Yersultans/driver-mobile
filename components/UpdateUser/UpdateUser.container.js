import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { Alert } from 'react-native'
import { gql, useMutation } from '@apollo/client'
import moment from 'moment'
import PropTypes from 'prop-types'
import localization from 'moment/locale/ru'
import HeaderNav from '../shared/HeaderNav'
import UpdateUser from './UpdateUser.design'

import StatusBarCustom from '../shared/StatusBarCustom'
import { useAuth } from '../context/useAuth'
import THEME from '../shared/theme'

const UPDATE_USER = gql`
  mutation updateUser($id: ID!, $input: UserInput) {
    updateUser(id: $id, input: $input) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
    }
  }
`

const UpdateUserContainer = ({ route }) => {
  const { userId } = route.params
  const { user, fetchUser } = useAuth()
  const [username, setUsername] = React.useState('')
  const [firstname, setFirstname] = React.useState('')
  const [lastname, setLastname] = React.useState('')
  const [phoneNumber, setPhoneNumber] = React.useState('')
  const [gender, setGender] = React.useState('')
  moment.updateLocale('ru', localization)
  const navigation = useNavigation()
  const [updateUser, { data, loading, error }] = useMutation(UPDATE_USER, {
    onCompleted() {
      fetchUser()
      Alert.alert(
        'Successfully',
        'Information about you successfully updated',
        [
          {
            text: 'Сancel',
            style: 'cancel'
          }
        ]
      )
    }
  })

  React.useEffect(() => {
    if (user) {
      setUsername(user?.username)
      setFirstname(user?.firstname)
      setLastname(user?.lastname)
      setPhoneNumber(user?.phoneNumber)
      setGender(user?.gender)
    }
  }, [user])

  const handleUpdate = () => {
    updateUser({
      variables: {
        id: userId,
        input: {
          username,
          firstname,
          lastname,
          phoneNumber,
          gender,
          role: 'user'
        }
      },
      errorPolicy: 'all'
    })
  }
  return (
    <>
      <StatusBarCustom />
      <HeaderNav
        title="Update User Info"
        backgroundColor="rgb(245 245 248)"
        onPress={() => navigation.goBack()}
        withNextBtn
        onPressNext={() => {
          handleUpdate()
        }}
        nextBtnTitle="change"
        nextBtnColor={THEME.COLOR_MAIN}
      />
      <UpdateUser
        user={user}
        username={username}
        setUsername={setUsername}
        firstname={firstname}
        setFirstname={setFirstname}
        lastname={lastname}
        setLastname={setLastname}
        phoneNumber={phoneNumber}
        setPhoneNumber={setPhoneNumber}
        gender={gender}
        setGender={setGender}
      />
    </>
  )
}

UpdateUserContainer.propTypes = {
  route: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)).isRequired
}

export default UpdateUserContainer

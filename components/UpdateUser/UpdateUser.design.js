import React from 'react'
import { View, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native'
import PropTypes from 'prop-types'

import THEME from '../shared/theme'
import RegisterFormItem from '../shared/RegisterForm/RegisterFormItem'
import StatusBarCustom from '../shared/StatusBarCustom'

const Login = ({
  username,
  setUsername,
  firstname,
  setFirstname,
  lastname,
  setLastname,
  phoneNumber,
  setPhoneNumber,
  gender,
  setGender
}) => {
  const emailRef = React.useRef()
  const firstnameRef = React.useRef()
  const lastnameRef = React.useRef()
  const phoneNumberRef = React.useRef()
  const genderRef = React.useRef()

  return (
    <>
      <StatusBarCustom backgroundColor={THEME.COLOR_WHITE} />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' && 'padding'}
      >
        <View
          style={{
            ...styles.container,
            marginTop: 32
          }}
        >
          <View style={styles.inputsContainer}>
            <RegisterFormItem
              title="Email"
              autoCapitalize="none"
              placeholder="Email"
              reference={emailRef}
              value={username}
              onPress={() => emailRef.current.focus()}
              onChangeText={text => setUsername(text.trim().toLowerCase())}
            />
            <RegisterFormItem
              title="Firstname"
              autoCapitalize="none"
              placeholder="Firstname"
              reference={firstnameRef}
              value={firstname}
              onPress={() => firstnameRef.current.focus()}
              onChangeText={text => setFirstname(text.trim().toLowerCase())}
            />
            <RegisterFormItem
              title="Lastname"
              autoCapitalize="none"
              placeholder="Lastname"
              reference={lastnameRef}
              value={lastname}
              onPress={() => lastnameRef.current.focus()}
              onChangeText={text => setLastname(text.trim().toLowerCase())}
            />
            <RegisterFormItem
              title="Phone Number"
              autoCapitalize="none"
              placeholder="Phone Number"
              reference={phoneNumberRef}
              value={phoneNumber}
              onPress={() => phoneNumberRef.current.focus()}
              onChangeText={text => setPhoneNumber(text.trim().toLowerCase())}
            />
            <RegisterFormItem
              title="Gender"
              reference={genderRef}
              onPress={() => genderRef.current.togglePicker()}
              withPicker
              onValueChange={text => setGender(text)}
              items={[
                { label: 'Male', value: 'male' },
                { label: 'Female', value: 'female' }
              ]}
              pickerPlaceholder={{
                label: 'Choose gender',
                value: ''
              }}
              value={gender}
            />
          </View>
        </View>
        {/* <View style={styles.button}>
          <Button
            onPress={handleSubmit}
            paddingVertical={24}
            backgroundColor={THEME.COLOR_MAIN}
            disabled={!(username && password)}
          >
            Registation
          </Button>
        </View> */}
      </KeyboardAvoidingView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {},
  inputsContainer: {
    // marginHorizontal: 50
  },
  forgot: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 40
  },
  inner: {
    marginBottom: 40
  },
  eula: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 32
  },
  warning: {
    marginHorizontal: 24,
    textAlign: 'center',
    marginBottom: 6
  },
  button: {
    paddingBottom: 50,
    marginHorizontal: 24
  }
})

Login.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isError: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
  setUsername: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
  setPassword: PropTypes.func.isRequired,
  firstname: PropTypes.string.isRequired,
  setFirstname: PropTypes.func.isRequired,
  lastname: PropTypes.string.isRequired,
  setLastname: PropTypes.func.isRequired,
  phoneNumber: PropTypes.string.isRequired,
  setPhoneNumber: PropTypes.func.isRequired,
  gender: PropTypes.string.isRequired,
  setGender: PropTypes.func.isRequired
}

export default Login

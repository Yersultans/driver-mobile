import React from 'react'
import PropTypes from 'prop-types'
import {
  gql,
  useMutation,
  useQuery,
  useLazyQuery,
  useSubscription
} from '@apollo/client'
import AsyncStorage from '@react-native-async-storage/async-storage'

const GET_CURRENT_USER = gql`
  query getCurrentUser {
    getCurrentUser {
      id
      role
      username
      lastname
      firstname
      phoneNumber
      avatarUrl
    }
  }
`

const LOGOUT = gql`
  mutation logout {
    logout {
      message
    }
  }
`

const DELETE_USER_EXPO_PUSH = gql`
  mutation deleteUserExpoToken($input: UserExpoTokenInput) {
    deleteUserExpoToken(input: $input) {
      message
    }
  }
`

const SUBSCRIBE_TO_ONLINE_ORDER = gql`
  subscription {
    order {
      mutation
      data {
        id
        status
      }
    }
  }
`

const SUBSCRIBE_TO_ONLINE_MESSAGE = gql`
  subscription {
    userMessage {
      mutation
      data {
        id
        order {
          id
        }
      }
    }
  }
`
const UNREAD_MESSAGES = gql`
  query unreadUserMessage($userId: ID) {
    unreadUserMessage(userId: $userId) {
      id
      text
      isReaded
      fromUser {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      toUser {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      order {
        id
      }
    }
  }
`
const ACTIVE_ORDER = gql`
  query activeOrder($userId: ID) {
    activeOrder(userId: $userId) {
      id
      formAddress {
        street
        latitude
        longitude
      }
      toAddress {
        street
        latitude
        longitude
      }
      driver {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      status
      price
      paymentMethod
    }
  }
`

const DRIVER_ACTIVE_ORDER = gql`
  query activeDriverOrder($userId: ID) {
    activeDriverOrder(userId: $userId) {
      id
      formAddress {
        street
        latitude
        longitude
      }
      toAddress {
        street
        latitude
        longitude
      }
      client {
        id
        username
        lastname
        firstname
        phoneNumber
      }
      status
      price
      paymentMethod
    }
  }
`

const authContext = React.createContext()

// Provider hook that creates auth object and handles state
function useProvideAuth() {
  const [activeOrder, setActiveOrder] = React.useState(null)
  const [driverActiveOrder, setDriverActiveOrder] = React.useState(null)
  const [messages, setMessages] = React.useState([])
  const [user, setUser] = React.useState(null)

  const [sendLogout] = useMutation(LOGOUT)
  const [deleteUserExpoToken] = useMutation(DELETE_USER_EXPO_PUSH)

  const { data, loading, error, refetch, networkStatus } = useQuery(
    GET_CURRENT_USER,
    {
      notifyOnNetworkStatusChange: false
    }
  )

  const [
    getActiveOrder,
    {
      data: dataActiveOrder,
      loading: loadingActiveOrder,
      error: errorActiveOrder,
      refetch: refetchActiveOrder
    }
  ] = useLazyQuery(ACTIVE_ORDER, {
    fetchPolicy: 'no-cache'
  })

  const [
    getMessages,
    {
      data: dataMessages,
      loading: loadingMessages,
      error: errorMessages,
      refetch: refetchMessages
    }
  ] = useLazyQuery(UNREAD_MESSAGES, {
    fetchPolicy: 'no-cache'
  })

  const [
    getDriverActiveOrder,
    {
      data: dataDriverActiveOrder,
      loading: loadingDriverActiveOrder,
      error: errorDriverActiveOrder,
      refetch: refetchDriverActiveOrder
    }
  ] = useLazyQuery(DRIVER_ACTIVE_ORDER, {
    fetchPolicy: 'no-cache'
  })

  const {
    data: dataSub,
    error: errorSub,
    loading: loadingSub
  } = useSubscription(SUBSCRIBE_TO_ONLINE_ORDER)

  const {
    data: dataSubMessage,
    error: errorSubMessage,
    loading: loadingSubMessage
  } = useSubscription(SUBSCRIBE_TO_ONLINE_MESSAGE)

  const getTips = async () => {
    const jsonValue = await AsyncStorage.getItem('tips')
    return jsonValue !== null ? JSON.parse(jsonValue) : null
  }

  const storeTips = async value => {
    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem('tips', jsonValue)
  }

  React.useEffect(() => {
    if (!loading && data && data.getCurrentUser) {
      setUser(data.getCurrentUser)
      if (data.getCurrentUser?.id) {
        getActiveOrder({
          variables: { userId: data.getCurrentUser?.id }
        })
        getDriverActiveOrder({
          variables: { userId: data.getCurrentUser?.id }
        })
        getMessages({
          variables: { userId: data.getCurrentUser?.id }
        })
      }
    }
  }, [data, loading, error, networkStatus])

  React.useEffect(() => {
    if (!loadingActiveOrder && dataActiveOrder) {
      setActiveOrder(dataActiveOrder.activeOrder)
    }
  }, [dataActiveOrder, loadingActiveOrder, errorActiveOrder])

  React.useEffect(() => {
    if (!loadingMessages && dataMessages) {
      setMessages(dataMessages.unreadUserMessage)
    }
  }, [dataMessages, loadingMessages, errorMessages])

  React.useEffect(() => {
    if (!loadingDriverActiveOrder && dataDriverActiveOrder) {
      setDriverActiveOrder(dataDriverActiveOrder.activeDriverOrder)
    }
  }, [dataDriverActiveOrder, loadingDriverActiveOrder, errorDriverActiveOrder])

  async function fetchUser() {
    await refetch()
  }

  const logout = async () => {
    sendLogout()
    deleteUserExpoToken({
      variables: { input: { userId: user.id } }
    })
    await AsyncStorage.removeItem('token')
    setUser(null)
  }

  const checkUserIsLoggedIn = async () => {
    const token = await AsyncStorage.getItem('token')
    return token !== null
  }

  // Return the user object and auth methods
  return {
    user,
    checkUserIsLoggedIn,
    fetchUser,
    logout,
    loading,
    storeTips,
    getTips,
    refetch,
    activeOrder,
    dataSub,
    errorSub,
    loadingSub,
    dataSubMessage,
    errorSubMessage,
    loadingSubMessage,
    driverActiveOrder,
    refetchActiveOrder,
    refetchDriverActiveOrder,
    messages,
    refetchMessages
  }
}

// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export function ProvideAuth({ children }) {
  const auth = useProvideAuth()
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
  return React.useContext(authContext)
}

ProvideAuth.propTypes = {
  children: PropTypes.node.isRequired
}

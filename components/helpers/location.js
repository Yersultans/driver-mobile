const getStreetName = async ({ latitude, longitude }) => {
  await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyCRAVPjFPFutjCGlWwo96OJV1yYlWJiw-4`
  )
    .then(response => response.json())
    .then(responseJson => {
      responseJson.results.map(result => {
        if (result.types.includes('street_address')) {
          console.log('result', result.formatted_address)
          return result.formatted_address
        }
      })
    })
}

export default getStreetName

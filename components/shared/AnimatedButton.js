import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TapGestureHandler } from 'react-native-gesture-handler'
import Animated, {
  measure,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedRef,
  useAnimatedStyle,
  useSharedValue,
  withTiming
} from 'react-native-reanimated'
import PropTypes from 'prop-types'

import THEME from './theme'

const AnimatedButton = ({
  children,
  onPress,
  propStyles,
  color = THEME.COLOR_WHITE,
  backgroundColor = THEME.COLOR_MAIN,
  paddingVertical,
  paddingHorizontal,
  fontSize,
  disabled,
  alignSelf
}) => {
  const centerX = useSharedValue(0)
  const centerY = useSharedValue(0)
  const scale = useSharedValue(0)
  const aRef = useAnimatedRef()
  const width = useSharedValue(0)
  const height = useSharedValue(0)
  const rippleOpacity = useSharedValue(1)

  const tapGestureEvent = useAnimatedGestureHandler({
    onStart: tapEvent => {
      const layout = measure(aRef)
      width.value = layout.width
      height.value = layout.height
      centerX.value = tapEvent.x
      centerY.value = tapEvent.y
      rippleOpacity.value = 1
      scale.value = 0
      scale.value = withTiming(1, { duration: 1000 })
    },
    onActive: () => {
      if (onPress && !disabled) runOnJS(onPress)()
    },
    onEnd: () => {
      rippleOpacity.value = withTiming(0)
    },
    onFail: () => {
      rippleOpacity.value = withTiming(0)
    },
    onCancel: () => {
      rippleOpacity.value = withTiming(0)
    },
    onFinish: () => {
      rippleOpacity.value = withTiming(0)
      // if (onPress && !disabled) runOnJS(onPress)()
    }
  })

  const rStyle = useAnimatedStyle(() => {
    const circleRadius = Math.sqrt(width.value ** 2.3 + height.value ** 2.3)
    const translateX = centerX.value - circleRadius
    const translateY = centerY.value - circleRadius

    return {
      width: circleRadius * 2,
      height: circleRadius * 2,
      borderRadius: circleRadius,
      backgroundColor: 'rgba(0,0,0,0.2)',
      position: 'absolute',
      opacity: rippleOpacity.value,
      top: 0,
      left: 0,
      alignSelf: 'center',
      transform: [
        { translateX },
        { translateY },
        {
          scale: scale.value
        }
      ]
    }
  })

  return (
    <View>
      <TapGestureHandler
        onGestureEvent={tapGestureEvent}
        ref={aRef}
        disabled={disabled}
      >
        <Animated.View
          style={{
            ...styles.button,
            ...propStyles,
            backgroundColor,
            paddingVertical,
            paddingHorizontal,
            alignSelf
          }}
        >
          <View style={styles.btnContainer}>
            <Text
              style={{ ...styles.text, fontSize, color, lineHeight: fontSize }}
            >
              {children}
            </Text>
          </View>
          <Animated.View style={rStyle} />
        </Animated.View>
      </TapGestureHandler>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  btnContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontFamily: THEME.MEDIUM
  }
})

AnimatedButton.propTypes = {
  children: PropTypes.node.isRequired,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  paddingVertical: PropTypes.number,
  fontSize: PropTypes.number,
  alignSelf: PropTypes.string,
  paddingHorizontal: PropTypes.number,
  disabled: PropTypes.bool,
  propStyles: PropTypes.objectOf(PropTypes.any)
}

AnimatedButton.defaultProps = {
  color: THEME.COLOR_WHITE,
  backgroundColor: THEME.COLOR_MAIN,
  fontSize: 16,
  alignSelf: 'auto',
  paddingHorizontal: 0,
  propStyles: {},
  disabled: false,
  paddingVertical: 0
}

export default AnimatedButton

import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import ArrowLeft from '../../assets/svg/ArrowLeft'

const BackButton = ({ onPress, propStyles, color, paddingLeft }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.7}
      style={{
        ...styles.button,
        paddingLeft,
        ...propStyles
      }}
    >
      <ArrowLeft color={color} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    width: 50,
    height: 50,
    justifyContent: 'center'
  }
})

BackButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  propStyles: PropTypes.objectOf(PropTypes.any),
  paddingLeft: PropTypes.number
}

BackButton.defaultProps = {
  color: '#000',
  propStyles: {},
  paddingLeft: 16
}

export default BackButton

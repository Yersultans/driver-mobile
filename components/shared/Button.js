import React from 'react'
import { StyleSheet, View, TouchableHighlight, Text } from 'react-native'
import PropTypes from 'prop-types'
import THEME from './theme'

const Button = ({
  children,
  onPress,
  propStyles,
  color = THEME.COLOR_WHITE,
  backgroundColor = THEME.COLOR_MAIN,
  paddingVertical,
  paddingHorizontal,
  fontSize,
  disabled,
  alignSelf
}) => {
  return (
    <TouchableHighlight
      onPress={onPress}
      activeOpacity={0.8}
      underlayColor="transparent"
      disabled={disabled}
    >
      <View
        style={{
          ...styles.button,
          ...propStyles,
          backgroundColor,
          paddingVertical,
          paddingHorizontal,
          alignSelf
        }}
      >
        <Text style={{ ...styles.text, fontSize, color, lineHeight: fontSize }}>
          {children}
        </Text>
      </View>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontFamily: THEME.MEDIUM
  }
})

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  paddingVertical: PropTypes.number,
  fontSize: PropTypes.number,
  alignSelf: PropTypes.string,
  paddingHorizontal: PropTypes.number,
  disabled: PropTypes.bool,
  propStyles: PropTypes.objectOf(PropTypes.any)
}

Button.defaultProps = {
  color: THEME.COLOR_WHITE,
  backgroundColor: THEME.COLOR_MAIN,
  fontSize: 16,
  alignSelf: 'auto',
  paddingHorizontal: 0,
  propStyles: {},
  disabled: false,
  paddingVertical: 0
}

export default Button

import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from 'react-native'
import PropTypes from 'prop-types'

import BackButton from './BackButton'
import StepsIndicator from './RegisterForm/StepsIndicator'
import THEME from './theme'

const { width } = Dimensions.get('window')

const HeaderNav = ({
  title,
  color,
  onPress,
  backgroundColor,
  withoutBackButton,
  style,
  withNextBtn,
  onPressNext,
  nextBtnTitle,
  nextBtnColor,
  showIndicator,
  step,
  borderBottomWidth,
  fontSize
}) => {
  return (
    <View style={showIndicator ? { ...styles.indicator, backgroundColor } : {}}>
      <View
        style={{
          ...styles.header,
          ...style,
          backgroundColor,
          borderBottomWidth
        }}
      >
        <View
          style={[
            withNextBtn && styles.backButton,
            { opacity: withoutBackButton ? 0 : 1 }
          ]}
        >
          <BackButton color={color} onPress={onPress} />
        </View>
        <Text style={{ ...styles.headerTitle, color, fontSize }}>{title}</Text>
        {withNextBtn ? (
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={onPressNext}
            style={styles.nextBtn}
          >
            <Text style={{ ...styles.nextText, color: nextBtnColor }}>
              {nextBtnTitle}
            </Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.empty} />
        )}
      </View>
      {showIndicator && <StepsIndicator step={step} />}
    </View>
  )
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    height: 52,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: THEME.COLOR_BLACK_OPACITY_1
  },
  headerTitle: {
    // fontSize: width > 400 ? 18 : 14,
    lineHeight: 24,
    fontFamily: THEME.MEDIUM
  },
  backButton: {
    width: '30%'
  },
  empty: {
    width: 50,
    height: 30
  },
  nextText: {
    fontSize: width > 400 ? 16 : 14,
    lineHeight: 24,
    fontFamily: THEME.MEDIUM,
    marginLeft: 'auto'
  },
  nextBtn: {
    width: '30%',
    height: '100%',
    justifyContent: 'center',
    paddingRight: 16
  },
  indicator: {
    alignItems: 'center'
  }
})

HeaderNav.propTypes = {
  title: PropTypes.string,
  color: PropTypes.string,
  onPress: PropTypes.func,
  backgroundColor: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  withNextBtn: PropTypes.bool,
  onPressNext: PropTypes.func,
  nextBtnTitle: PropTypes.string,
  nextBtnColor: PropTypes.string,
  showIndicator: PropTypes.bool,
  step: PropTypes.number,
  borderBottomWidth: PropTypes.number,
  withoutBackButton: PropTypes.bool,
  fontSize: PropTypes.number
}

HeaderNav.defaultProps = {
  title: '',
  onPress: () => null,
  color: THEME.COLOR_BLACK,
  backgroundColor: THEME.COLOR_WHITE,
  style: {},
  withNextBtn: false,
  nextBtnTitle: 'Next',
  onPressNext: null,
  nextBtnColor: THEME.COLOR_BLACK_OPACITY_4,
  showIndicator: false,
  step: 1,
  borderBottomWidth: 0,
  withoutBackButton: false,
  fontSize: width > 400 ? 16 : 14
}

export default HeaderNav

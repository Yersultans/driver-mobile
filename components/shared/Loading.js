import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import THEME from '../shared/theme'

const Loading = ({ backgroundColor, propStyles }) => {
  return (
    <View style={{ ...styles.container, backgroundColor, ...propStyles }}>
      <ActivityIndicator size="large" color={THEME.COLOR_MAIN} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: THEME.COLOR_WHITE
  }
})

Loading.propTypes = {
  backgroundColor: PropTypes.string,
  propStyles: PropTypes.objectOf(PropTypes.any)
}

Loading.defaultProps = {
  backgroundColor: THEME.COLOR_WHITE,
  propStyles: {}
}

export default Loading

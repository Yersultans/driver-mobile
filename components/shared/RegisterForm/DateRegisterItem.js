import React from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  Platform,
  Modal,
  StyleSheet,
  Dimensions
} from 'react-native'
import PropTypes from 'prop-types'
import RNDateTimePicker from '@react-native-community/datetimepicker'

import CalendarIcon from '../../../assets/svg/Calendar'
import THEME from '../theme'
import dimensions from '../dimensions'

const DateRegisterItem = ({ birthday, setBirthday, isPlaceHolder }) => {
  const [showDate, setShowDate] = React.useState(false)
  const [defaultTime, setDefaultTime] = React.useState(true)
  const [date, setDate] = React.useState(new Date('December 17, 1995 03:24:00'))
  const onChange = (event, selectedDate) => {
    if (Platform.OS === 'android') {
      setShowDate(false)
    }
    if (selectedDate) {
      setBirthday(
        `${
          selectedDate.getDate() < 10
            ? `0${selectedDate.getDate()}`
            : `${selectedDate.getDate()}`
        }-${
          selectedDate.getMonth() + 1 < 10
            ? `0${selectedDate.getMonth() + 1}`
            : `${selectedDate.getMonth() + 1}`
        }-${selectedDate.getFullYear()}`
      )
      setDate(selectedDate)
      setDefaultTime(false)
    }
  }

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => setShowDate(true)}
    >
      <View style={styles.inner}>
        <CalendarIcon />
        <Text style={styles.text}>Birthday</Text>
      </View>
      <View>
        {defaultTime ? (
          <Text
            style={{
              ...styles.birthText,
              color: isPlaceHolder
                ? THEME.COLOR_BLACK_OPACITY_4
                : THEME.COLOR_BLACK
            }}
          >
            {birthday}
          </Text>
        ) : (
          <Text style={styles.birthText}>{birthday}</Text>
        )}
      </View>
      {showDate &&
        (Platform.OS === 'ios' ? (
          <Modal
            visible={showDate}
            transparent
            animationType="fade"
            onRequestClose={() => setShowDate(false)}
          >
            <TouchableOpacity
              style={styles.iosModal}
              onPress={() => setShowDate(false)}
            >
              <View
                style={{
                  width: Dimensions.get('window').width,
                  backgroundColor: THEME.COLOR_WHITE,
                  height: 300
                }}
              >
                <TouchableOpacity
                  onPress={() => setShowDate(false)}
                  style={{ height: 30, alignItems: 'flex-end' }}
                >
                  <Text
                    style={{
                      ...styles.text,
                      padding: 16
                    }}
                  >
                    Done
                  </Text>
                </TouchableOpacity>
                <RNDateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  is24Hour
                  mode="date"
                  display="spinner"
                  onChange={onChange}
                />
              </View>
            </TouchableOpacity>
          </Modal>
        ) : (
          <RNDateTimePicker
            testID="dateTimePicker"
            value={date}
            is24Hour
            mode="date"
            display="spinner"
            onChange={onChange}
            textColor="#000"
          />
        ))}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 64,
    backgroundColor: THEME.COLOR_WHITE,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1,
    paddingHorizontal: 16
  },
  inner: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontFamily: THEME.MEDIUM,
    fontSize: dimensions.height < 700 ? 12 : 16,
    lineHeight: 16,
    color: THEME.COLOR_BLACK,
    marginLeft: 16
  },
  birthText: {
    color: THEME.COLOR_BLACK,
    fontSize: dimensions.height < 700 ? 12 : 16,
    fontFamily: THEME.REGULAR,
    marginBottom: 5
  },
  iosModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: THEME.COLOR_BLACK_OPACITY_5
  }
})

DateRegisterItem.propTypes = {
  setBirthday: PropTypes.func.isRequired,
  birthday: PropTypes.string,
  isPlaceHolder: PropTypes.bool
}

DateRegisterItem.defaultProps = {
  isPlaceHolder: true,
  birthday: 'дд.мм.гггг'
}

export default DateRegisterItem

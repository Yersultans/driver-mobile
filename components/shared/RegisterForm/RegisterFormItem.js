import React from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from 'react-native'
import PropTypes from 'prop-types'
import RNPickerSelect from 'react-native-picker-select'
import MaskInput from 'react-native-mask-input'

import THEME from '../theme'
import ToggleShowPassword from './ToggleShowPassword'

const { height } = Dimensions.get('window').height

const RegisterFormItem = ({
  children,
  title,
  placeholder,
  onPress,
  onChangeText,
  onValueChange,
  onBlur,
  isPassword,
  reference,
  propStyles,
  keyBoardType,
  items,
  pickerPlaceholder,
  withIcon,
  withPicker,
  value,
  autoCapitalize,
  inputPaddingRight,
  phoneMask,
  maxLength
}) => {
  const [isShowPass, setIsShowPass] = React.useState(false)

  const renderTextInput = () => {
    if (phoneMask) {
      return (
        <MaskInput
          maxLength={17}
          ref={reference}
          secureTextEntry={isPassword && !isShowPass}
          keyboardType={keyBoardType}
          style={{ ...styles.input, paddingRight: inputPaddingRight }}
          onChangeText={onChangeText}
          value={value}
          onBlur={onBlur}
          autoCapitalize={autoCapitalize}
          mask={[
            '8',
            ' ',
            '(',
            /\d/, // that's because I want it to be a digit (0-9)
            /\d/,
            /\d/,
            ')',
            ' ',
            /\d/,
            /\d/,
            /\d/,
            '-',
            /\d/,
            /\d/,
            '-',
            /\d/,
            /\d/
          ]}
        />
      )
    }
    return (
      <TextInput
        ref={reference}
        secureTextEntry={isPassword && !isShowPass}
        placeholder={placeholder}
        keyboardType={keyBoardType}
        style={{ ...styles.input, paddingRight: inputPaddingRight }}
        onChangeText={onChangeText}
        value={value}
        onBlur={onBlur}
        maxLength={maxLength}
        autoCapitalize={autoCapitalize}
      />
    )
  }

  return (
    <TouchableOpacity
      style={{
        ...styles.container,
        ...propStyles
      }}
      activeOpacity={0.5}
      onPress={onPress}
    >
      {children}
      <Text
        style={{
          ...styles.text,
          marginLeft: withIcon ? 16 : 0
        }}
      >
        {title}
      </Text>
      {withPicker ? (
        <View style={styles.pickerContainer}>
          <RNPickerSelect
            ref={reference}
            pickerProps={{ selectedValue: value }}
            value={value}
            onValueChange={onValueChange}
            items={items}
            placeholder={pickerPlaceholder}
            style={{
              inputAndroid: {
                color: THEME.COLOR_BLACK,
                fontSize: height < 700 ? 12 : 16,
                textAlign: 'center',
                alignSelf: 'flex-end'
              },
              inputIOS: {
                color: THEME.COLOR_BLACK,
                fontSize: height < 700 ? 12 : 16,
                textAlign: 'center',
                alignSelf: 'flex-end'
              },
              placeholder: {
                fontFamily: THEME.REGULAR,
                color: THEME.COLOR_BLACK_OPACITY_4
              }
            }}
            Icon={() => false}
            useNativeAndroidPickerStyle={false}
          />
        </View>
      ) : (
        renderTextInput()
      )}
      {isPassword && (
        <ToggleShowPassword
          isShowPass={isShowPass}
          setIsShowPass={setIsShowPass}
        />
      )}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: height < 700 ? 56 : 64,
    backgroundColor: THEME.COLOR_WHITE,
    alignItems: 'center',
    paddingHorizontal: 16,
    borderTopColor: THEME.COLOR_BLACK_OPACITY_1,
    borderTopWidth: 1
  },
  text: {
    fontFamily: THEME.MEDIUM,
    fontSize: height < 700 ? 12 : 16,
    lineHeight: 16,
    color: THEME.COLOR_BLACK,
    marginLeft: 16,
    marginRight: 12
  },
  input: {
    fontFamily: THEME.REGULAR,
    flex: 1,
    fontSize: height < 700 ? 12 : 16
  },
  pickerContainer: {
    flex: 1
  }
})

RegisterFormItem.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onPress: PropTypes.func,
  onChangeText: PropTypes.func,
  onValueChange: PropTypes.func,
  isPassword: PropTypes.bool,
  reference: PropTypes.objectOf(PropTypes.any),
  propStyles: PropTypes.objectOf(PropTypes.any),
  keyBoardType: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.any),
  pickerPlaceholder: PropTypes.objectOf(PropTypes.any),
  withIcon: PropTypes.bool,
  withPicker: PropTypes.bool,
  value: PropTypes.string,
  onBlur: PropTypes.func,
  autoCapitalize: PropTypes.string,
  inputPaddingRight: PropTypes.number,
  maxLength: PropTypes.number
}

RegisterFormItem.defaultProps = {
  children: null,
  placeholder: 'Placeholder',
  onPress: null,
  onValueChange: () => null,
  onChangeText: () => null,
  isPassword: false,
  reference: null,
  propStyles: {},
  keyBoardType: 'default',
  items: [],
  pickerPlaceholder: {},
  withIcon: true,
  withPicker: false,
  value: null,
  onBlur: null,
  autoCapitalize: 'none',
  inputPaddingRight: 20,
  maxLength: null
}

export default RegisterFormItem

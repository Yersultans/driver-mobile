import React from 'react'
import { View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import THEME from '../theme'

const StepsIndicator = ({ step }) => {
  const steps = () => {
    if (step === 1) {
      return (
        <View style={styles.stepsContainer}>
          <View style={styles.dot1} />
          <View style={styles.regLine1} />
          <View style={styles.dot3} />
          <View style={styles.regLine1} />
          <View style={styles.dot3} />
        </View>
      )
    } else if (step === 2) {
      return (
        <View style={styles.stepsContainer}>
          <View style={styles.dot2} />
          <View style={styles.regLine2} />
          <View style={styles.dot1} />
          <View style={styles.regLine1} />
          <View style={styles.dot3} />
        </View>
      )
    }
    return (
      <View style={styles.stepsContainer}>
        <View style={styles.dot2} />
        <View style={styles.regLine2} />
        <View style={styles.dot2} />
        <View style={styles.regLine2} />
        <View style={styles.dot1} />
      </View>
    )
  }
  return <View>{steps()}</View>
}

const styles = StyleSheet.create({
  dot1: {
    width: 16,
    height: 16,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: THEME.COLOR_GREEN
  },
  dot2: {
    width: 16,
    height: 16,
    borderRadius: 8,
    backgroundColor: THEME.COLOR_GREEN
  },
  dot3: {
    width: 16,
    height: 16,
    borderRadius: 8,
    backgroundColor: THEME.COLOR_GRAY
  },
  regLine1: {
    position: 'relative',
    height: 6,
    flex: 1,
    backgroundColor: THEME.COLOR_GRAY
  },
  regLine2: {
    position: 'relative',
    height: 6,
    flex: 1,
    backgroundColor: THEME.COLOR_GREEN
  },
  stepsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '50%',
    paddingVertical: 8
  }
})

StepsIndicator.propTypes = {
  step: PropTypes.number.isRequired
}

export default StepsIndicator

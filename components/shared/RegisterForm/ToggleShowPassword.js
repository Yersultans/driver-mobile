import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import ShowPasswordIcon from '../../../assets/svg/ShowPasswordIcon'
import HidePasswordIcon from '../../../assets/svg/HidePasswordIcon'

const ToggleShowPassword = ({ isShowPass, setIsShowPass }) => {
  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.5}
      onPress={() => setIsShowPass(!isShowPass)}
    >
      {isShowPass ? <ShowPasswordIcon /> : <HidePasswordIcon />}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: 0,
    width: 50,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

ToggleShowPassword.propTypes = {
  isShowPass: PropTypes.bool.isRequired,
  setIsShowPass: PropTypes.func.isRequired
}

export default ToggleShowPassword

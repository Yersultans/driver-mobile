import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import THEME from '../theme'

const ValidationError = ({ error, backgroundColor, propStyles }) => {
  return (
    <View style={{ ...styles.container, ...propStyles, backgroundColor }}>
      <Text style={styles.text}>{error}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 32,
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  text: {
    color: THEME.COLOR_WHITE,
    fontSize: 14,
    fontFamily: THEME.REGULAR
  }
})

ValidationError.propTypes = {
  error: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  propStyles: PropTypes.objectOf(PropTypes.any)
}

ValidationError.defaultProps = {
  backgroundColor: '#FF5C5C',
  propStyles: {}
}

export default ValidationError

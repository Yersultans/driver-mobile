import React from 'react'
import { View, StatusBar, Platform } from 'react-native'
import PropTypes from 'prop-types'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import THEME from './theme'

const StatusBarCustom = ({
  backgroundColor,
  propStyles,
  barStyle,
  translucent,
  hidden
}) => {
  return (
    <>
      {Platform.OS === 'ios' ? (
        <View
          style={{
            height: hidden ? 0 : getStatusBarHeight(),
            backgroundColor,
            ...propStyles
          }}
        >
          <StatusBar barStyle={barStyle} hidden={hidden} />
        </View>
      ) : (
        <StatusBar
          backgroundColor={backgroundColor}
          barStyle={barStyle}
          translucent={translucent}
          hidden={hidden}
        />
      )}
    </>
  )
}

StatusBarCustom.propTypes = {
  backgroundColor: PropTypes.string,
  barStyle: PropTypes.string,
  propStyles: PropTypes.objectOf(PropTypes.any),
  translucent: PropTypes.bool,
  hidden: PropTypes.bool
}

StatusBarCustom.defaultProps = {
  barStyle: 'dark-content',
  backgroundColor: THEME.COLOR_WHITE,
  propStyles: {},
  translucent: false,
  hidden: false
}

export default StatusBarCustom
